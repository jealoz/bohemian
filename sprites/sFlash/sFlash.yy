{
    "id": "22a1f827-75cb-48f1-9e69-384805960fc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f98b22f-2963-4b32-a538-a89f5b3dfc39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a1f827-75cb-48f1-9e69-384805960fc3",
            "compositeImage": {
                "id": "30e24a81-27c9-43b9-9a5c-25e9a61eb43c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f98b22f-2963-4b32-a538-a89f5b3dfc39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c91cc21-c0fa-4eb1-b25f-44dccee2fafe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f98b22f-2963-4b32-a538-a89f5b3dfc39",
                    "LayerId": "61b5dadd-bbe7-42ac-b322-78a3da020597"
                }
            ]
        },
        {
            "id": "084a0f32-0c17-485b-84a8-8a8dcf973cd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a1f827-75cb-48f1-9e69-384805960fc3",
            "compositeImage": {
                "id": "51fd46da-ae0a-4d55-b5c1-7d44ca715754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084a0f32-0c17-485b-84a8-8a8dcf973cd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c09b36-de14-42d6-aa5b-b20c585557e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084a0f32-0c17-485b-84a8-8a8dcf973cd9",
                    "LayerId": "61b5dadd-bbe7-42ac-b322-78a3da020597"
                }
            ]
        },
        {
            "id": "db7e0e2a-f2ff-43be-adee-9591a9a2992d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a1f827-75cb-48f1-9e69-384805960fc3",
            "compositeImage": {
                "id": "a59de13f-839a-4859-af4d-2804fc40f862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7e0e2a-f2ff-43be-adee-9591a9a2992d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bddc682-36d1-4525-aecb-bfe4720cabda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7e0e2a-f2ff-43be-adee-9591a9a2992d",
                    "LayerId": "61b5dadd-bbe7-42ac-b322-78a3da020597"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "61b5dadd-bbe7-42ac-b322-78a3da020597",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22a1f827-75cb-48f1-9e69-384805960fc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 4,
    "yorig": 17
}