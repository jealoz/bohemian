{
    "id": "45f1c6f1-9a5d-48d5-95f8-5865ffa806e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlash2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 17,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5288431b-2922-4f1e-8076-90381dd40ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f1c6f1-9a5d-48d5-95f8-5865ffa806e7",
            "compositeImage": {
                "id": "bdca27b6-2f48-448a-b9c9-070ef3bb8a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5288431b-2922-4f1e-8076-90381dd40ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca41de58-a891-41bf-ac6d-f7d5bbdb105b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5288431b-2922-4f1e-8076-90381dd40ac1",
                    "LayerId": "b1d949c4-d33c-48d2-bc2d-35f8a8bdc0a0"
                }
            ]
        },
        {
            "id": "9f9a4bce-8ac1-47bd-9774-86a6170ed745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f1c6f1-9a5d-48d5-95f8-5865ffa806e7",
            "compositeImage": {
                "id": "04ea88fd-8bbf-4a48-9e2c-19256ccedc1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9a4bce-8ac1-47bd-9774-86a6170ed745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005ac44e-24c5-4323-b77b-ef190909d7f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9a4bce-8ac1-47bd-9774-86a6170ed745",
                    "LayerId": "b1d949c4-d33c-48d2-bc2d-35f8a8bdc0a0"
                }
            ]
        },
        {
            "id": "c5e81879-c767-413c-814d-5e2bc72db0b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f1c6f1-9a5d-48d5-95f8-5865ffa806e7",
            "compositeImage": {
                "id": "d2d8b570-e1f3-4048-ac46-50641681ba98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5e81879-c767-413c-814d-5e2bc72db0b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "922ea6fc-e352-44ff-b423-5ff5b96417a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5e81879-c767-413c-814d-5e2bc72db0b6",
                    "LayerId": "b1d949c4-d33c-48d2-bc2d-35f8a8bdc0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "b1d949c4-d33c-48d2-bc2d-35f8a8bdc0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45f1c6f1-9a5d-48d5-95f8-5865ffa806e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 15
}