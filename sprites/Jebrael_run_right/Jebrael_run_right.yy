{
    "id": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 754,
    "bbox_left": 290,
    "bbox_right": 595,
    "bbox_top": 697,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3a87d92-b497-47fa-b2eb-579f1c22e9a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "4ecef78b-05f1-45e0-9315-8b2e7f2dd076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a87d92-b497-47fa-b2eb-579f1c22e9a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fac29e3-ddd1-47f6-b6e7-c1963dd2f8d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a87d92-b497-47fa-b2eb-579f1c22e9a2",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "687291b4-2503-4e37-b645-7b26dce19f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "2b4d647b-8d43-413a-bf3c-1867df8a3f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "687291b4-2503-4e37-b645-7b26dce19f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7fecee9-0251-4bca-a593-5507331243a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "687291b4-2503-4e37-b645-7b26dce19f47",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "fdea823d-ef59-424d-97db-c349eb6de292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "15569406-5d36-4bbe-87b8-f6e0f85abdf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdea823d-ef59-424d-97db-c349eb6de292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed082f3c-a2bf-4ea6-bd21-78ffccc84617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdea823d-ef59-424d-97db-c349eb6de292",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "445a205b-eb41-45d1-9093-b85851ed9075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "b207705d-b46e-4be5-a0aa-6fa592c1f50f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "445a205b-eb41-45d1-9093-b85851ed9075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "869f7520-0315-4c43-bcef-ac45c7015404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "445a205b-eb41-45d1-9093-b85851ed9075",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "3bb91fb9-00db-4acf-a671-c37592e2bab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "d7f70b8d-4c73-4fd6-a809-b6acfe014e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bb91fb9-00db-4acf-a671-c37592e2bab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2989a1f-c2b8-4b66-9e2b-0b562e44804f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bb91fb9-00db-4acf-a671-c37592e2bab9",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "46b1632e-924f-4faa-99f6-45f0d13372b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "9dfa0636-e3d0-49fb-83aa-537e014f9fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b1632e-924f-4faa-99f6-45f0d13372b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4a5605-bc22-4aab-aa7e-0f1e1c9a9486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b1632e-924f-4faa-99f6-45f0d13372b3",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "478a2cad-f148-4db4-ae11-646b3c254d44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "350ebae5-9bcf-440d-a066-e435f0c9405d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "478a2cad-f148-4db4-ae11-646b3c254d44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9764087b-323c-484f-957d-80066dc0a17f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "478a2cad-f148-4db4-ae11-646b3c254d44",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "3bf2b5fc-8f4d-4752-8372-4fbab9af8bf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "63f80257-2c02-4701-9a02-f2a8a3e7b2b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf2b5fc-8f4d-4752-8372-4fbab9af8bf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf6c89d-a9ae-4322-ad79-80cdd0f74b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf2b5fc-8f4d-4752-8372-4fbab9af8bf9",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "6335a6e0-0d76-46af-aa5b-7d5e57d1c30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "c6789b05-e777-425c-b0b7-cb2cccb4550c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6335a6e0-0d76-46af-aa5b-7d5e57d1c30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "659f5c3e-4d85-40fd-b7e2-c3f02b36a504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6335a6e0-0d76-46af-aa5b-7d5e57d1c30b",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "58d68db8-3df4-4224-818e-d9e86bdc6d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "58b800cb-d2df-43ca-8b3b-121eee711fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d68db8-3df4-4224-818e-d9e86bdc6d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616e04da-e03b-45a7-8256-c2d8d4c513a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d68db8-3df4-4224-818e-d9e86bdc6d65",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "78c6ab88-dc9d-4c88-b95e-2dc7685f4fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "61243043-3060-44a7-bd90-de85142d6fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78c6ab88-dc9d-4c88-b95e-2dc7685f4fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a82a44-a3b9-45be-bcbc-811d0be43c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78c6ab88-dc9d-4c88-b95e-2dc7685f4fa3",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        },
        {
            "id": "1b433d33-9aa2-4160-835a-6e2527a8c0d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "compositeImage": {
                "id": "62b5837a-ca7b-4f9e-bbc4-090de3ab9836",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b433d33-9aa2-4160-835a-6e2527a8c0d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a9c197-e361-4092-9319-999cb37f6638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b433d33-9aa2-4160-835a-6e2527a8c0d0",
                    "LayerId": "b276cd18-e69f-47c1-8401-6f04dbf135ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "b276cd18-e69f-47c1-8401-6f04dbf135ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26d641e1-657c-4a65-9ac9-a9649ed9865e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 457,
    "yorig": 596
}