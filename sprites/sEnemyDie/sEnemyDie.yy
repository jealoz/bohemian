{
    "id": "ae4bc264-bf65-4ef5-946f-573252bcff35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 98,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9e836e7-51e3-4e1f-9539-5ea8abcacb29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4bc264-bf65-4ef5-946f-573252bcff35",
            "compositeImage": {
                "id": "d4759ec0-db29-47f9-9843-b6ffbfb37411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e836e7-51e3-4e1f-9539-5ea8abcacb29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6cc266d-c885-4a6c-bc57-d28c91b78b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e836e7-51e3-4e1f-9539-5ea8abcacb29",
                    "LayerId": "15002a7f-8904-40d0-8c79-dc1bb9ef3094"
                }
            ]
        },
        {
            "id": "5688422f-d4b5-42a1-b7c9-8918c0b7b4fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4bc264-bf65-4ef5-946f-573252bcff35",
            "compositeImage": {
                "id": "629b51fb-2817-41ba-b499-8a01b7756c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5688422f-d4b5-42a1-b7c9-8918c0b7b4fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47c33e47-c712-46be-9355-599412fb01ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5688422f-d4b5-42a1-b7c9-8918c0b7b4fb",
                    "LayerId": "15002a7f-8904-40d0-8c79-dc1bb9ef3094"
                }
            ]
        },
        {
            "id": "ebf5c56d-f58e-4609-99d0-53212d43af09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4bc264-bf65-4ef5-946f-573252bcff35",
            "compositeImage": {
                "id": "7de6fb8b-154b-42a9-b651-14a6c96f59bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf5c56d-f58e-4609-99d0-53212d43af09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae95addc-1c79-4879-846a-4e760cd207a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf5c56d-f58e-4609-99d0-53212d43af09",
                    "LayerId": "15002a7f-8904-40d0-8c79-dc1bb9ef3094"
                }
            ]
        },
        {
            "id": "b65412c3-ca02-46c3-a2f7-732f7b73d037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4bc264-bf65-4ef5-946f-573252bcff35",
            "compositeImage": {
                "id": "f54b63c5-9b76-4b0b-8c8e-0014c559c308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65412c3-ca02-46c3-a2f7-732f7b73d037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd29ab85-342b-40bd-accd-48958b865350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65412c3-ca02-46c3-a2f7-732f7b73d037",
                    "LayerId": "15002a7f-8904-40d0-8c79-dc1bb9ef3094"
                }
            ]
        },
        {
            "id": "62f4c2b4-6a49-4440-b949-d5cb9a985aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4bc264-bf65-4ef5-946f-573252bcff35",
            "compositeImage": {
                "id": "a061beed-b454-44d7-85f4-816d452d973c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f4c2b4-6a49-4440-b949-d5cb9a985aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd82d866-6b52-48ec-98d9-617e5189ebfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f4c2b4-6a49-4440-b949-d5cb9a985aad",
                    "LayerId": "15002a7f-8904-40d0-8c79-dc1bb9ef3094"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "15002a7f-8904-40d0-8c79-dc1bb9ef3094",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae4bc264-bf65-4ef5-946f-573252bcff35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 58,
    "yorig": 62
}