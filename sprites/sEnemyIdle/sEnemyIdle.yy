{
    "id": "a1836692-823f-40b0-8703-c8e7ac9fc3e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7393c38f-b894-4cb6-a345-f427d2abc204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1836692-823f-40b0-8703-c8e7ac9fc3e9",
            "compositeImage": {
                "id": "328ac8cf-6cdd-495c-9be0-d98ac699b4a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7393c38f-b894-4cb6-a345-f427d2abc204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a3d072-ecee-46e5-b2f8-11c8f5842f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7393c38f-b894-4cb6-a345-f427d2abc204",
                    "LayerId": "1246c4e1-7702-4f57-ab40-2b696799784e"
                }
            ]
        },
        {
            "id": "63b877b7-4fff-414c-9c3e-cdab42a604eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1836692-823f-40b0-8703-c8e7ac9fc3e9",
            "compositeImage": {
                "id": "4d23a097-6f13-4d7d-9d37-4facd0b6d9d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b877b7-4fff-414c-9c3e-cdab42a604eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdd19580-9189-410c-b531-b075f3389e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b877b7-4fff-414c-9c3e-cdab42a604eb",
                    "LayerId": "1246c4e1-7702-4f57-ab40-2b696799784e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1246c4e1-7702-4f57-ab40-2b696799784e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1836692-823f-40b0-8703-c8e7ac9fc3e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 42,
    "yorig": 61
}