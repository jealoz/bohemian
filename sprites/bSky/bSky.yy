{
    "id": "cd80724c-0634-4882-8abc-ec0ea7897511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bSky",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae9fb760-2661-4944-9936-c4d518b6a28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd80724c-0634-4882-8abc-ec0ea7897511",
            "compositeImage": {
                "id": "c16d05a6-59f1-406c-a8de-49447d2b354f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9fb760-2661-4944-9936-c4d518b6a28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2ab0925-5524-4d5a-b9ae-8c780d974dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9fb760-2661-4944-9936-c4d518b6a28d",
                    "LayerId": "41b948dc-9246-475c-b2b4-84e1f49173ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1025,
    "layers": [
        {
            "id": "41b948dc-9246-475c-b2b4-84e1f49173ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd80724c-0634-4882-8abc-ec0ea7897511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}