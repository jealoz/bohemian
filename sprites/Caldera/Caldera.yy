{
    "id": "731ff4a8-48d0-4253-aa71-344f6c7f92ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Caldera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52b636b8-1d8e-477a-8edf-50f4ee1e34ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "731ff4a8-48d0-4253-aa71-344f6c7f92ce",
            "compositeImage": {
                "id": "4ad9ad76-a1de-45d7-aabc-8ae62fe16a72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b636b8-1d8e-477a-8edf-50f4ee1e34ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62124e1f-c343-4720-88e0-2eb018454010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b636b8-1d8e-477a-8edf-50f4ee1e34ca",
                    "LayerId": "bb754aef-0549-462e-9a23-6a0fc01b0c96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "bb754aef-0549-462e-9a23-6a0fc01b0c96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "731ff4a8-48d0-4253-aa71-344f6c7f92ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}