{
    "id": "33ba47be-62dc-4e8e-a813-3967b8e65083",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 13,
    "bbox_right": 50,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d773520-bcd1-46d5-830e-0f6bf83fce99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ba47be-62dc-4e8e-a813-3967b8e65083",
            "compositeImage": {
                "id": "65912200-46ae-40ea-9dc1-5737ba59e24a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d773520-bcd1-46d5-830e-0f6bf83fce99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f72cd87-d31a-4a2d-95f2-18a36758f0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d773520-bcd1-46d5-830e-0f6bf83fce99",
                    "LayerId": "1e3f5ff3-dc2a-4934-aeba-ace90de7516c"
                }
            ]
        },
        {
            "id": "dcf8700d-88b6-4016-8b24-b2308940982a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ba47be-62dc-4e8e-a813-3967b8e65083",
            "compositeImage": {
                "id": "d67f23c2-8672-4322-b30c-6771daba735a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf8700d-88b6-4016-8b24-b2308940982a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6f98a5-e3aa-4069-869c-79f2e9124615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf8700d-88b6-4016-8b24-b2308940982a",
                    "LayerId": "1e3f5ff3-dc2a-4934-aeba-ace90de7516c"
                }
            ]
        },
        {
            "id": "1673c9a3-8182-4dbf-908e-36b66679b19a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ba47be-62dc-4e8e-a813-3967b8e65083",
            "compositeImage": {
                "id": "2ead8e8a-14c5-4dc7-ade5-bd115f8eb297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1673c9a3-8182-4dbf-908e-36b66679b19a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dad9457-bf50-4728-b7ad-be4db241a5f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1673c9a3-8182-4dbf-908e-36b66679b19a",
                    "LayerId": "1e3f5ff3-dc2a-4934-aeba-ace90de7516c"
                }
            ]
        },
        {
            "id": "fc3a7f96-d2e1-44e2-9287-95ba68b8d256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ba47be-62dc-4e8e-a813-3967b8e65083",
            "compositeImage": {
                "id": "aa942fca-655f-4ded-89db-5336893cf088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3a7f96-d2e1-44e2-9287-95ba68b8d256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e76c9dd-0c14-434f-95e0-ff8b53278d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3a7f96-d2e1-44e2-9287-95ba68b8d256",
                    "LayerId": "1e3f5ff3-dc2a-4934-aeba-ace90de7516c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "1e3f5ff3-dc2a-4934-aeba-ace90de7516c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33ba47be-62dc-4e8e-a813-3967b8e65083",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 33,
    "yorig": 76
}