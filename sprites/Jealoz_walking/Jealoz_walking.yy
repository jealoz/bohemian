{
    "id": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jealoz_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 747,
    "bbox_left": 356,
    "bbox_right": 543,
    "bbox_top": 182,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a46e867-c982-4065-b495-dfb2e39946a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "7a925be0-589e-4aa4-805d-3c6288b8be75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a46e867-c982-4065-b495-dfb2e39946a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26db2eb0-5036-45ea-a61a-18e90496c695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a46e867-c982-4065-b495-dfb2e39946a5",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "4fa067ec-2e95-4e16-bf73-cf6ee3b0662d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "c6bc47e9-63c5-4dce-b2c3-0c23942b7ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa067ec-2e95-4e16-bf73-cf6ee3b0662d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f614103-2cd2-408b-b3ae-e3c0bd25f00a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa067ec-2e95-4e16-bf73-cf6ee3b0662d",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "a9eab8db-0a33-46e6-8de7-ce9e22eb3468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "08ae7e05-9937-4d69-8be4-4b7dd55ca9d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9eab8db-0a33-46e6-8de7-ce9e22eb3468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bbbf55a-9c21-4f05-b989-63be3675103d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9eab8db-0a33-46e6-8de7-ce9e22eb3468",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "c3ccb82c-b47d-4a05-a8db-61c2b5033587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "ac5cffe7-f687-4fd8-81c1-018b6d0d0de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ccb82c-b47d-4a05-a8db-61c2b5033587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f0bce2-178e-495c-83a9-9d324865848e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ccb82c-b47d-4a05-a8db-61c2b5033587",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "1706b7cf-32c5-4bdf-9bed-6440369907ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "e05a343e-6c63-40ce-a37b-06a3b264e125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1706b7cf-32c5-4bdf-9bed-6440369907ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8a82a4-42f0-4d37-80b3-7146c6413e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1706b7cf-32c5-4bdf-9bed-6440369907ad",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "17f73b30-853f-446c-95af-8ab148d9df84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "b1ef1ac9-8053-4070-b28c-608401cbbcbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f73b30-853f-446c-95af-8ab148d9df84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dad2001-00f8-4c5f-a996-8f1ccc3fc5f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f73b30-853f-446c-95af-8ab148d9df84",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "18db29ce-bc30-477e-9d5c-127b7c169e07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "1a9a785b-85c3-4032-a185-e8d79e5b5250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18db29ce-bc30-477e-9d5c-127b7c169e07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4733b390-b915-4bd9-b444-5887bf267bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18db29ce-bc30-477e-9d5c-127b7c169e07",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "c4615933-fcb5-44d4-bf41-dc3196ed56e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "285b5a3e-b0a3-4217-994d-0b4a98206654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4615933-fcb5-44d4-bf41-dc3196ed56e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5407b725-4560-4d8b-9851-eb96e81b83ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4615933-fcb5-44d4-bf41-dc3196ed56e3",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "cfc2066e-d74a-4aa0-a04a-82cd79a55905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "800a691d-8dec-495c-a984-c4136a364051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfc2066e-d74a-4aa0-a04a-82cd79a55905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a0466db-91ee-4240-bfd1-72ceb46e3a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfc2066e-d74a-4aa0-a04a-82cd79a55905",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "10f918b6-0824-440e-97a5-0bc3af74c352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "65d63444-3dbf-4350-938f-91b350e7f716",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f918b6-0824-440e-97a5-0bc3af74c352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3722b420-ebee-4472-b9ea-70b1df73ba07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f918b6-0824-440e-97a5-0bc3af74c352",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "d0a11587-69ca-4bb9-a9a9-ea1372f84907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "1afe4526-3243-407a-a444-9c8f8917fe03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a11587-69ca-4bb9-a9a9-ea1372f84907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a48ac23c-c480-4e54-a24c-652a54a66421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a11587-69ca-4bb9-a9a9-ea1372f84907",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "3de5c73a-67c9-41b4-8e43-fe5c21dfee83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "31041b9c-b116-40e0-a8bd-dd257791d287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de5c73a-67c9-41b4-8e43-fe5c21dfee83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e97b741a-52dc-4f17-a91c-0060d35bf02b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de5c73a-67c9-41b4-8e43-fe5c21dfee83",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "652ecf38-a47e-483c-8c05-bd605e45af3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "c544b9d6-e6ad-4f28-a566-757dca14fa29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652ecf38-a47e-483c-8c05-bd605e45af3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "467378b0-7830-4314-ab0c-0e7f1762ae86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652ecf38-a47e-483c-8c05-bd605e45af3c",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "4d10a1e8-e954-483e-bd77-b51c0e4a2e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "8840b575-2bcb-4f54-89e6-9d5f1527af9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d10a1e8-e954-483e-bd77-b51c0e4a2e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "325563bf-3666-4c88-8f86-f84f3960e1a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d10a1e8-e954-483e-bd77-b51c0e4a2e2d",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "05ccd4c0-9d08-49fc-9bc7-f5ba32ffbe27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "8134632f-c7ab-4235-abaa-a44b161534e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05ccd4c0-9d08-49fc-9bc7-f5ba32ffbe27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "435f3ddc-fdec-4c42-b274-544f82aade18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05ccd4c0-9d08-49fc-9bc7-f5ba32ffbe27",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "be722af1-adab-4c14-8d70-9f590986c0b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "0d2958c4-b683-44fa-a973-219cc7ffe135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be722af1-adab-4c14-8d70-9f590986c0b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54c61b0-19c1-4b47-bafd-1bb159e6da99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be722af1-adab-4c14-8d70-9f590986c0b1",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "38095e70-de18-4508-a2ad-951f7a3aa7f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "b3fbd1f7-4ed2-4bfd-9ead-ce9b8c0e8099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38095e70-de18-4508-a2ad-951f7a3aa7f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1cba9d-a867-4330-b6cd-3ad928de07c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38095e70-de18-4508-a2ad-951f7a3aa7f7",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "89a98417-1b97-47c9-9b7a-85e05acbd59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "0b86336b-7108-4779-868f-b8c41240e9a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a98417-1b97-47c9-9b7a-85e05acbd59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b111489-f531-4fc2-857f-38e63d982b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a98417-1b97-47c9-9b7a-85e05acbd59d",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "6725968b-cb31-4d51-9580-c1e6a7ebfdfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "85d4a94f-6d91-4b61-8149-ab54bed8c4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6725968b-cb31-4d51-9580-c1e6a7ebfdfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a17deeb-06ca-4bee-b388-ac52e5616599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6725968b-cb31-4d51-9580-c1e6a7ebfdfe",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "275b90dd-2130-4e75-8ba6-6aa5429c4a7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "40c8297f-20db-4b51-844a-7ac9a8f37ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275b90dd-2130-4e75-8ba6-6aa5429c4a7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42754aad-cc6e-4c5a-b561-e90ddeb3ea9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275b90dd-2130-4e75-8ba6-6aa5429c4a7e",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "46b2072f-c7c7-4415-af97-dcf6b6301eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "b8e1eaee-9d1c-40fe-82e9-13c3d3746d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b2072f-c7c7-4415-af97-dcf6b6301eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5eaf86-7904-4317-86be-07ca3f7ae6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b2072f-c7c7-4415-af97-dcf6b6301eef",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "e7d29458-9ca5-4931-9686-03eee25db207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "ecd29d88-be9f-4e8b-83ef-5bcfd7d46203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d29458-9ca5-4931-9686-03eee25db207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a36ba10-ecfe-40d7-b760-33f7844d466d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d29458-9ca5-4931-9686-03eee25db207",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "12068fb3-96e1-4f43-b435-9706b32209eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "0e92168b-ce7a-4fb2-a94d-fbfd0bbb8ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12068fb3-96e1-4f43-b435-9706b32209eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a23e10-a10b-420d-8fb0-4776da98537f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12068fb3-96e1-4f43-b435-9706b32209eb",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        },
        {
            "id": "974cb75d-10de-479d-8c15-f45b71ef9fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "compositeImage": {
                "id": "1197a71f-409e-4eca-a48c-413800c20ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974cb75d-10de-479d-8c15-f45b71ef9fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "379bb35f-0e23-4615-8283-4193381a21be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974cb75d-10de-479d-8c15-f45b71ef9fcd",
                    "LayerId": "54cfe5c5-0ead-4875-a300-37aa2824d15f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "54cfe5c5-0ead-4875-a300-37aa2824d15f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61a0ebbd-cc5d-4627-a1fb-281680249cc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 450
}