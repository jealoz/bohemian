{
    "id": "3cebd1dd-eb0f-425b-b372-af92026f3acb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJumpDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 3,
    "bbox_right": 99,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccc4662a-152d-4a95-8064-7e8a52fc759c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cebd1dd-eb0f-425b-b372-af92026f3acb",
            "compositeImage": {
                "id": "bf458bb2-32b8-4106-9507-af274e5d3e3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc4662a-152d-4a95-8064-7e8a52fc759c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4608b7bb-788f-46b6-8794-a31a050ff6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc4662a-152d-4a95-8064-7e8a52fc759c",
                    "LayerId": "3964856a-6bab-4afd-b4b5-b44ffebdab21"
                }
            ]
        },
        {
            "id": "cda61bf0-4b5e-4cf1-955d-f1186d4d5326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cebd1dd-eb0f-425b-b372-af92026f3acb",
            "compositeImage": {
                "id": "1aa823e6-d380-4adc-8789-814bdb0a04ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda61bf0-4b5e-4cf1-955d-f1186d4d5326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59bb14ad-f9fd-415f-ac96-c5778c565813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda61bf0-4b5e-4cf1-955d-f1186d4d5326",
                    "LayerId": "3964856a-6bab-4afd-b4b5-b44ffebdab21"
                }
            ]
        },
        {
            "id": "a1d19ff3-2040-41d7-8a1f-5ca7c7947bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cebd1dd-eb0f-425b-b372-af92026f3acb",
            "compositeImage": {
                "id": "51d1353f-51b7-4822-9883-d43a3fa1ac00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d19ff3-2040-41d7-8a1f-5ca7c7947bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be01183-5847-40dd-aa16-dadb728cf4c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d19ff3-2040-41d7-8a1f-5ca7c7947bab",
                    "LayerId": "3964856a-6bab-4afd-b4b5-b44ffebdab21"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3964856a-6bab-4afd-b4b5-b44ffebdab21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cebd1dd-eb0f-425b-b372-af92026f3acb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 15,
    "yorig": 96
}