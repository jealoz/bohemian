{
    "id": "ea438791-6ccf-451b-8cff-7858e9316b53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jealoz_atack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 753,
    "bbox_left": 212,
    "bbox_right": 854,
    "bbox_top": 183,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0dedc81-0fb9-4c93-aa8c-b4a98fcdcd00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "5f2a5057-bb33-44c5-a24e-9b32250441ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0dedc81-0fb9-4c93-aa8c-b4a98fcdcd00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e96b42d-cb8a-46aa-93cb-ef35dca0ee83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0dedc81-0fb9-4c93-aa8c-b4a98fcdcd00",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "fb05465b-543c-4a8e-8544-ae8c890b9e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "0f615937-f0d8-4863-8a6e-c2336260ebd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb05465b-543c-4a8e-8544-ae8c890b9e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb612976-a6a1-45f8-b832-d5303a2b692e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb05465b-543c-4a8e-8544-ae8c890b9e95",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "0201d8ce-f0de-4e45-9b45-adca10ee72cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "082247dd-5bf5-49e9-9a0c-d69c7fe472c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0201d8ce-f0de-4e45-9b45-adca10ee72cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77922ba3-ef54-4706-b536-b5c10f269348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0201d8ce-f0de-4e45-9b45-adca10ee72cc",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "d1bc3690-c335-47f8-ae04-113290d86816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "af04936c-c6e1-4ec0-ad81-b195835fd17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1bc3690-c335-47f8-ae04-113290d86816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ab1031-51fc-4cfb-ab13-80e350e02636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1bc3690-c335-47f8-ae04-113290d86816",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "4b5cca24-b75a-43ce-860a-eadc03837d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "7d871bfb-5ca6-40e5-a938-e013aa80dda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5cca24-b75a-43ce-860a-eadc03837d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "386813eb-0377-46e9-bf60-acb820ae3ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5cca24-b75a-43ce-860a-eadc03837d86",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "8b660f19-f21c-4471-bdb2-f0c7abf2adf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "46eeb4d8-31d1-4700-b253-a00dc97781d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b660f19-f21c-4471-bdb2-f0c7abf2adf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c82658f-46f4-44cf-ad26-7a007478b458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b660f19-f21c-4471-bdb2-f0c7abf2adf7",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "21827741-e03b-40b1-8e6d-a30bb6bd9a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "92aa666f-601c-4a85-ad7a-59126eb41952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21827741-e03b-40b1-8e6d-a30bb6bd9a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ac4a20-4050-42e2-8f16-5d8993263e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21827741-e03b-40b1-8e6d-a30bb6bd9a3f",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "a9c74c98-fb2e-44ba-ae78-07ec5f7d2fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "1f01b398-ea2c-4a3f-8e61-bcfc3a7aa7b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c74c98-fb2e-44ba-ae78-07ec5f7d2fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4ab18f4-a05c-4079-ac85-25e081352c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c74c98-fb2e-44ba-ae78-07ec5f7d2fcc",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "7f70d148-28b0-4358-9029-63b2a7c03ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "f0aebb65-763b-4e96-ba03-a8cb53f0b1b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f70d148-28b0-4358-9029-63b2a7c03ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7991e41e-b9d4-448d-8625-882ab8839bf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f70d148-28b0-4358-9029-63b2a7c03ce8",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "cee77082-5d51-42be-94fd-3da5ad5d69f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "b32f392a-cdf6-4e21-8a2d-fc0f38cfd016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee77082-5d51-42be-94fd-3da5ad5d69f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d7a3ac-dbea-4557-a77e-96a3dddb249f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee77082-5d51-42be-94fd-3da5ad5d69f4",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "f7f6bbc3-343e-4b8c-b0c3-95da26a42e64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "a34cc9e7-5d71-4be6-afac-88b1c651d03d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f6bbc3-343e-4b8c-b0c3-95da26a42e64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69f0612f-412d-4d2f-8df2-b1c9b38fba3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f6bbc3-343e-4b8c-b0c3-95da26a42e64",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        },
        {
            "id": "95c79d1d-f82f-4a80-8e48-c5b805575630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "compositeImage": {
                "id": "49c739bd-6f8a-4b3c-ad79-804e65772c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95c79d1d-f82f-4a80-8e48-c5b805575630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c30f5db-93df-495e-8737-62c987c06494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95c79d1d-f82f-4a80-8e48-c5b805575630",
                    "LayerId": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "e7283eb0-2b29-4cf5-b75b-0d710f9a18c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea438791-6ccf-451b-8cff-7858e9316b53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 450
}