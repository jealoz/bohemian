{
    "id": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jealoz_cayendo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 669,
    "bbox_left": 313,
    "bbox_right": 606,
    "bbox_top": 165,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4600c96-421c-49e2-8df9-846380e60290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "compositeImage": {
                "id": "4f728454-678d-4e71-8e03-dc56b4ca1490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4600c96-421c-49e2-8df9-846380e60290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb59943-928a-4d0c-9e44-a3c8d7a22975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4600c96-421c-49e2-8df9-846380e60290",
                    "LayerId": "25d0075b-d5fe-4317-b731-9d3111465281"
                }
            ]
        },
        {
            "id": "b49820f3-fca0-4b0a-91b5-7ec5a9314b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "compositeImage": {
                "id": "22cde686-7a97-4d62-8628-6e9f779054ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b49820f3-fca0-4b0a-91b5-7ec5a9314b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9693e8f7-47b7-4c5b-966f-8aaf5a0afa4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b49820f3-fca0-4b0a-91b5-7ec5a9314b9a",
                    "LayerId": "25d0075b-d5fe-4317-b731-9d3111465281"
                }
            ]
        },
        {
            "id": "c1cfcec7-fc30-433b-ad07-57c332a39a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "compositeImage": {
                "id": "50d2c06e-9258-4d7d-8f11-283f51c0a752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1cfcec7-fc30-433b-ad07-57c332a39a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7806dc-34fe-44b1-a04f-9559e78932c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1cfcec7-fc30-433b-ad07-57c332a39a6d",
                    "LayerId": "25d0075b-d5fe-4317-b731-9d3111465281"
                }
            ]
        },
        {
            "id": "0889d8b5-6fff-4398-8dc6-9484dce8a293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "compositeImage": {
                "id": "fcb83978-5d1f-4b1c-9e19-bba24bdb803c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0889d8b5-6fff-4398-8dc6-9484dce8a293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee123ee2-f27b-460c-b626-ed940f3cdf3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0889d8b5-6fff-4398-8dc6-9484dce8a293",
                    "LayerId": "25d0075b-d5fe-4317-b731-9d3111465281"
                }
            ]
        },
        {
            "id": "447a6047-5742-4d07-a3ec-c892993bbbf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "compositeImage": {
                "id": "ee4a04d8-2de8-413d-9ee7-da71ba223046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "447a6047-5742-4d07-a3ec-c892993bbbf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e627a40b-1ddc-445b-b6f9-89562ede0ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "447a6047-5742-4d07-a3ec-c892993bbbf3",
                    "LayerId": "25d0075b-d5fe-4317-b731-9d3111465281"
                }
            ]
        },
        {
            "id": "cdebdc3f-a95d-466d-9d29-935842f4fda9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "compositeImage": {
                "id": "ee326c60-98a3-4b2d-af81-361c2dce4ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdebdc3f-a95d-466d-9d29-935842f4fda9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5059f6db-bdd4-458d-b057-bf4bed9cde9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdebdc3f-a95d-466d-9d29-935842f4fda9",
                    "LayerId": "25d0075b-d5fe-4317-b731-9d3111465281"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "25d0075b-d5fe-4317-b731-9d3111465281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ee90c55-b4a2-494b-9405-4aee5d9beb5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}