{
    "id": "61ab958e-41c5-4e82-9044-16b0205762b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAttack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 3,
    "bbox_right": 77,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7544c654-873c-4b6c-9bec-f6c08e29c861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61ab958e-41c5-4e82-9044-16b0205762b3",
            "compositeImage": {
                "id": "6b772c30-116a-480f-907f-0a929881bd5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7544c654-873c-4b6c-9bec-f6c08e29c861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a234361a-b55c-4464-8158-b96073b3c559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7544c654-873c-4b6c-9bec-f6c08e29c861",
                    "LayerId": "45f5861b-d6fb-48da-9b95-74baad1a5578"
                }
            ]
        },
        {
            "id": "541d4729-dd4c-4a39-8f04-7d31eea5b7a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61ab958e-41c5-4e82-9044-16b0205762b3",
            "compositeImage": {
                "id": "067a4439-af96-441a-b633-dcb3c2537894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "541d4729-dd4c-4a39-8f04-7d31eea5b7a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcabcfe-b2d1-4021-87c1-913e35e5c694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "541d4729-dd4c-4a39-8f04-7d31eea5b7a1",
                    "LayerId": "45f5861b-d6fb-48da-9b95-74baad1a5578"
                }
            ]
        },
        {
            "id": "490120ce-f5e3-479c-a44a-5ec00a8a436d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61ab958e-41c5-4e82-9044-16b0205762b3",
            "compositeImage": {
                "id": "d9b14407-1ba1-474a-87ba-8abf485336b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "490120ce-f5e3-479c-a44a-5ec00a8a436d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfa8c97-b3a9-4575-a133-b2d1d647aa14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "490120ce-f5e3-479c-a44a-5ec00a8a436d",
                    "LayerId": "45f5861b-d6fb-48da-9b95-74baad1a5578"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "45f5861b-d6fb-48da-9b95-74baad1a5578",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61ab958e-41c5-4e82-9044-16b0205762b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 30,
    "yorig": 77
}