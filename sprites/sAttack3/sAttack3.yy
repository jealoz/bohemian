{
    "id": "845de451-116e-4a37-a3ee-6165787a04a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAttack3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 7,
    "bbox_right": 129,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7703d16c-1220-46c7-9da3-1260850d9db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845de451-116e-4a37-a3ee-6165787a04a5",
            "compositeImage": {
                "id": "bb47ce5f-be32-4920-83cb-14622d5f577d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7703d16c-1220-46c7-9da3-1260850d9db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df40692c-b589-4ae4-87ec-8ea51c039fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7703d16c-1220-46c7-9da3-1260850d9db5",
                    "LayerId": "4964ad84-7fef-4ede-b2bb-c9bb8c03acdf"
                }
            ]
        },
        {
            "id": "c50b23eb-a573-4cbf-bb38-6c0eeccf74e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845de451-116e-4a37-a3ee-6165787a04a5",
            "compositeImage": {
                "id": "0c6ba676-62a4-4ae5-8cf0-86fc3bc2c62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50b23eb-a573-4cbf-bb38-6c0eeccf74e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d6d00c5-1b99-4aed-ab87-76dd7c172687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50b23eb-a573-4cbf-bb38-6c0eeccf74e2",
                    "LayerId": "4964ad84-7fef-4ede-b2bb-c9bb8c03acdf"
                }
            ]
        },
        {
            "id": "c2aef4d9-0444-4315-9eb9-0476f3d04c41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845de451-116e-4a37-a3ee-6165787a04a5",
            "compositeImage": {
                "id": "bd162d71-be10-48c0-a7f8-e0ce95899a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2aef4d9-0444-4315-9eb9-0476f3d04c41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58be5d32-0fa5-4a40-81f4-babebf2105af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2aef4d9-0444-4315-9eb9-0476f3d04c41",
                    "LayerId": "4964ad84-7fef-4ede-b2bb-c9bb8c03acdf"
                }
            ]
        },
        {
            "id": "dffda1b3-3a8f-4d10-9134-e8bbcd92be13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845de451-116e-4a37-a3ee-6165787a04a5",
            "compositeImage": {
                "id": "d0f5b55f-d55e-44fc-a299-2360d77baf61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dffda1b3-3a8f-4d10-9134-e8bbcd92be13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c3f4b02-37fe-43ca-ae49-b5b3ea06c073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dffda1b3-3a8f-4d10-9134-e8bbcd92be13",
                    "LayerId": "4964ad84-7fef-4ede-b2bb-c9bb8c03acdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "4964ad84-7fef-4ede-b2bb-c9bb8c03acdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "845de451-116e-4a37-a3ee-6165787a04a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 73,
    "yorig": 71
}