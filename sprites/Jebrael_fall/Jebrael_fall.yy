{
    "id": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 716,
    "bbox_left": 162,
    "bbox_right": 620,
    "bbox_top": 127,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb320d74-4a21-4cde-ad81-7dfbc5cd5fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "compositeImage": {
                "id": "700299ab-da38-4075-8b7f-85c95fb916a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb320d74-4a21-4cde-ad81-7dfbc5cd5fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c98c8579-6401-460f-8117-e0d65bb2dea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb320d74-4a21-4cde-ad81-7dfbc5cd5fc1",
                    "LayerId": "f0e7876e-bc65-42ad-8c10-6048617ee338"
                }
            ]
        },
        {
            "id": "fcd3b867-1910-46c0-8a04-870027941bec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "compositeImage": {
                "id": "b4b27285-d08e-4066-82b1-bf2a84aa1ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd3b867-1910-46c0-8a04-870027941bec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8800790a-3f01-4daa-94cd-d3e8a6e4ea70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd3b867-1910-46c0-8a04-870027941bec",
                    "LayerId": "f0e7876e-bc65-42ad-8c10-6048617ee338"
                }
            ]
        },
        {
            "id": "b112b740-4716-4c11-8c77-ed4eaba9b35e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "compositeImage": {
                "id": "d8625993-9d07-4c42-a37b-780c6133adc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b112b740-4716-4c11-8c77-ed4eaba9b35e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d4b703d-00ed-447c-84b8-3026e31923d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b112b740-4716-4c11-8c77-ed4eaba9b35e",
                    "LayerId": "f0e7876e-bc65-42ad-8c10-6048617ee338"
                }
            ]
        },
        {
            "id": "6bceadf1-0528-407f-acba-285fc087d9a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "compositeImage": {
                "id": "fb968577-db28-44cf-a918-357f1ce4bfe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bceadf1-0528-407f-acba-285fc087d9a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3ad7a85-28db-4235-adbf-eeaefe0f908b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bceadf1-0528-407f-acba-285fc087d9a9",
                    "LayerId": "f0e7876e-bc65-42ad-8c10-6048617ee338"
                }
            ]
        },
        {
            "id": "36866ae7-dee0-416e-8044-6ddd81f48e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "compositeImage": {
                "id": "d1681646-dbd7-4d6a-bc1a-4506260aa100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36866ae7-dee0-416e-8044-6ddd81f48e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e479a1c-74f4-4621-8251-69fa2fc293fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36866ae7-dee0-416e-8044-6ddd81f48e25",
                    "LayerId": "f0e7876e-bc65-42ad-8c10-6048617ee338"
                }
            ]
        },
        {
            "id": "e7ec22c6-fd39-4c55-8c34-6cdd67346d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "compositeImage": {
                "id": "336dcac5-baca-4ec2-b0fa-3f3ca74b68cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ec22c6-fd39-4c55-8c34-6cdd67346d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4faf0b77-128d-4413-bc35-24552ded08c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ec22c6-fd39-4c55-8c34-6cdd67346d3c",
                    "LayerId": "f0e7876e-bc65-42ad-8c10-6048617ee338"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "f0e7876e-bc65-42ad-8c10-6048617ee338",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a8290ed-ab41-412a-bc1e-2d733a518b9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}