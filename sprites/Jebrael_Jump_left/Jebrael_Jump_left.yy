{
    "id": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_Jump_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 750,
    "bbox_left": 240,
    "bbox_right": 727,
    "bbox_top": 136,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1c01213-c6b7-4471-a2aa-8f7aa0f145e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "compositeImage": {
                "id": "262fff96-a755-4cd3-979a-0a7b46b87373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c01213-c6b7-4471-a2aa-8f7aa0f145e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0592d68-a369-4fb1-8eda-878f51e58b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c01213-c6b7-4471-a2aa-8f7aa0f145e8",
                    "LayerId": "6c18f650-6187-492c-9630-87e446247f5a"
                }
            ]
        },
        {
            "id": "cf53c983-7726-4b31-ad5d-6faf856f844e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "compositeImage": {
                "id": "f557bd6e-80e4-4e89-9f58-8c347b859404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf53c983-7726-4b31-ad5d-6faf856f844e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbfbcaf3-1a9b-4bee-9b81-8df3aa2ed7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf53c983-7726-4b31-ad5d-6faf856f844e",
                    "LayerId": "6c18f650-6187-492c-9630-87e446247f5a"
                }
            ]
        },
        {
            "id": "1c5703a9-edc4-43c2-b441-c88496100d64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "compositeImage": {
                "id": "0e7f6839-bf81-4fa5-bdce-f8337925437e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c5703a9-edc4-43c2-b441-c88496100d64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "943665ac-9deb-4eee-8246-d368396fb209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c5703a9-edc4-43c2-b441-c88496100d64",
                    "LayerId": "6c18f650-6187-492c-9630-87e446247f5a"
                }
            ]
        },
        {
            "id": "7402af7e-f824-43d4-9cd4-f0263f7389ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "compositeImage": {
                "id": "8eee9f19-ee47-4436-82f8-b09cbf00a41c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7402af7e-f824-43d4-9cd4-f0263f7389ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14ff1871-1d35-4ae3-9e15-dd3d2032a357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7402af7e-f824-43d4-9cd4-f0263f7389ed",
                    "LayerId": "6c18f650-6187-492c-9630-87e446247f5a"
                }
            ]
        },
        {
            "id": "1e7a9168-fd2a-4cdf-a2b0-5c7db7ca8c45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "compositeImage": {
                "id": "4b24efc5-fbf3-42a4-8096-67ac44489b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e7a9168-fd2a-4cdf-a2b0-5c7db7ca8c45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e1e3214-7d70-4593-8ade-18515d6f29f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e7a9168-fd2a-4cdf-a2b0-5c7db7ca8c45",
                    "LayerId": "6c18f650-6187-492c-9630-87e446247f5a"
                }
            ]
        },
        {
            "id": "4c9f3007-8f5a-42b2-be6b-ea455f433937",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "compositeImage": {
                "id": "3e7810d1-5285-4aa5-870e-2d3ce201ae3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9f3007-8f5a-42b2-be6b-ea455f433937",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc4ae0e-352c-4181-abb9-ccfdefc02505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9f3007-8f5a-42b2-be6b-ea455f433937",
                    "LayerId": "6c18f650-6187-492c-9630-87e446247f5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "6c18f650-6187-492c-9630-87e446247f5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06427fff-4f6a-41f3-be69-d1d3128fa94e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}