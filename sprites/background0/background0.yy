{
    "id": "3d2e7c7c-c7c4-44d1-ac7b-8c4ae961124f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background0",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a21cc82-73bc-4f58-baf5-cf6486602aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d2e7c7c-c7c4-44d1-ac7b-8c4ae961124f",
            "compositeImage": {
                "id": "ffeb72a2-bd69-40fa-bffc-22079d6462fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a21cc82-73bc-4f58-baf5-cf6486602aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa0cbed-716b-41c3-9fbb-042351d2353d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a21cc82-73bc-4f58-baf5-cf6486602aa4",
                    "LayerId": "b4b68d08-d49e-43b7-8b67-83c2ef5f82e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "b4b68d08-d49e-43b7-8b67-83c2ef5f82e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d2e7c7c-c7c4-44d1-ac7b-8c4ae961124f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}