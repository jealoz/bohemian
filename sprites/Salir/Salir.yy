{
    "id": "c0fbfbad-43d2-454a-8448-ed9e7e082201",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Salir",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a393131b-f423-411b-85ca-ba28dd934d8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fbfbad-43d2-454a-8448-ed9e7e082201",
            "compositeImage": {
                "id": "825913bd-d5d1-4627-ba56-d27668b4bcff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a393131b-f423-411b-85ca-ba28dd934d8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2296725-3bb0-4d3b-a5b3-b6af6e5cb997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a393131b-f423-411b-85ca-ba28dd934d8a",
                    "LayerId": "396df384-b2d9-493b-af42-d6488c47c301"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "396df384-b2d9-493b-af42-d6488c47c301",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0fbfbad-43d2-454a-8448-ed9e7e082201",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 29
}