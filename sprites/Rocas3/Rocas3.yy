{
    "id": "54c005c9-005b-41bd-9b03-564bf5194ec0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rocas3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cd2bd0e-c933-4306-90d5-3f4f95f0f892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c005c9-005b-41bd-9b03-564bf5194ec0",
            "compositeImage": {
                "id": "d1636d1b-1651-4b5b-a886-0444361a66ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cd2bd0e-c933-4306-90d5-3f4f95f0f892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c808c0f1-dcba-4bfd-b9e0-c3066b6d5d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cd2bd0e-c933-4306-90d5-3f4f95f0f892",
                    "LayerId": "d4fadb19-9661-4d9f-8eff-a33b8dc7a45e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d4fadb19-9661-4d9f-8eff-a33b8dc7a45e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54c005c9-005b-41bd-9b03-564bf5194ec0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 23
}