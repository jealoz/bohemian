{
    "id": "ab7c3676-b134-4c64-9577-3d043a57744b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rocas2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d874b342-fd43-44e4-8e02-09fb8dec1b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab7c3676-b134-4c64-9577-3d043a57744b",
            "compositeImage": {
                "id": "8ee5c975-e041-4b3b-b933-3e672d454335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d874b342-fd43-44e4-8e02-09fb8dec1b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ed7ec4-c996-462d-b2ae-76479beae188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d874b342-fd43-44e4-8e02-09fb8dec1b06",
                    "LayerId": "91925225-a856-46e8-b46e-0346a07de2ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "91925225-a856-46e8-b46e-0346a07de2ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab7c3676-b134-4c64-9577-3d043a57744b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 46,
    "yorig": 33
}