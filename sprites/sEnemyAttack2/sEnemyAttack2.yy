{
    "id": "79350e2a-5534-4a9f-ab40-d0613a28504a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyAttack2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 5,
    "bbox_right": 75,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "830db90e-e1ff-431b-aac1-dc5fabba286f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79350e2a-5534-4a9f-ab40-d0613a28504a",
            "compositeImage": {
                "id": "3cb4ea54-34fc-4241-839f-9745f2bac5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830db90e-e1ff-431b-aac1-dc5fabba286f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b91cf6-8c6d-4b94-99bd-825c31993b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830db90e-e1ff-431b-aac1-dc5fabba286f",
                    "LayerId": "ee24613d-4686-489f-9614-a1c77ac7b49a"
                }
            ]
        },
        {
            "id": "481f8406-986e-4f88-8d49-cec74444f69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79350e2a-5534-4a9f-ab40-d0613a28504a",
            "compositeImage": {
                "id": "0a97761d-3463-4f55-a0e7-7d2eee3e00c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "481f8406-986e-4f88-8d49-cec74444f69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce1c2ad3-995b-4eab-bf73-4b18e45adcc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481f8406-986e-4f88-8d49-cec74444f69c",
                    "LayerId": "ee24613d-4686-489f-9614-a1c77ac7b49a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "ee24613d-4686-489f-9614-a1c77ac7b49a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79350e2a-5534-4a9f-ab40-d0613a28504a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 58,
    "yorig": 61
}