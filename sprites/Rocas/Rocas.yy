{
    "id": "f72bf634-6b93-4b0f-aa28-116dda0792c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rocas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 8,
    "bbox_right": 70,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc5b9bea-7c34-475f-a189-b1d1237be684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f72bf634-6b93-4b0f-aa28-116dda0792c9",
            "compositeImage": {
                "id": "0c3f2456-170b-41a9-a20f-3065b5c11d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc5b9bea-7c34-475f-a189-b1d1237be684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5b990c-2385-4cbd-b888-1c5622cf1086",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc5b9bea-7c34-475f-a189-b1d1237be684",
                    "LayerId": "a9ab6922-e90a-4c41-babd-6f28b19115e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a9ab6922-e90a-4c41-babd-6f28b19115e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f72bf634-6b93-4b0f-aa28-116dda0792c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 46,
    "yorig": 33
}