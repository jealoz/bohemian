{
    "id": "e5630324-e949-4f9e-a1c2-5d42aab6a91e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyAttack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 9,
    "bbox_right": 57,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c74e946-8f9e-4433-b912-46fcc4af986f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5630324-e949-4f9e-a1c2-5d42aab6a91e",
            "compositeImage": {
                "id": "7497b725-8a45-4695-8449-62bc398fd15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c74e946-8f9e-4433-b912-46fcc4af986f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c3f205-cadb-48c5-a2a0-f59d8d586e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c74e946-8f9e-4433-b912-46fcc4af986f",
                    "LayerId": "bb7e0c75-48f3-49ab-ab36-751c6db2d646"
                }
            ]
        },
        {
            "id": "f1f8faee-539e-4d16-a0e2-2b34aefd30e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5630324-e949-4f9e-a1c2-5d42aab6a91e",
            "compositeImage": {
                "id": "eaf50500-335a-4c74-b50b-5dae1bdb9825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f8faee-539e-4d16-a0e2-2b34aefd30e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334e9fb2-66d9-48ef-b888-25d89cc0d03a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f8faee-539e-4d16-a0e2-2b34aefd30e3",
                    "LayerId": "bb7e0c75-48f3-49ab-ab36-751c6db2d646"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "bb7e0c75-48f3-49ab-ab36-751c6db2d646",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5630324-e949-4f9e-a1c2-5d42aab6a91e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 36,
    "yorig": 76
}