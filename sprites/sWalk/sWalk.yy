{
    "id": "d49063b5-6fa1-44e9-af91-cf99b8ce8d98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 2,
    "bbox_right": 51,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "560a0288-078f-4f44-8d24-10a4fdc16b10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49063b5-6fa1-44e9-af91-cf99b8ce8d98",
            "compositeImage": {
                "id": "cbefea4d-5b53-48fb-a8e2-d29c120b894f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560a0288-078f-4f44-8d24-10a4fdc16b10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2e716e-ef13-4b52-8a50-e4067f2c78a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560a0288-078f-4f44-8d24-10a4fdc16b10",
                    "LayerId": "890cd01d-7adf-4bb6-82da-5e4845d2113a"
                }
            ]
        },
        {
            "id": "6803d51d-ab4e-48f1-85d4-464a432f0546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49063b5-6fa1-44e9-af91-cf99b8ce8d98",
            "compositeImage": {
                "id": "ef981b2d-1634-4b23-8f34-839bd5f67873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6803d51d-ab4e-48f1-85d4-464a432f0546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d8dc7a0-6bca-4703-b7b8-a60527d8d69e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6803d51d-ab4e-48f1-85d4-464a432f0546",
                    "LayerId": "890cd01d-7adf-4bb6-82da-5e4845d2113a"
                }
            ]
        },
        {
            "id": "74fced98-5ab3-4438-a74a-59d7b1e81efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49063b5-6fa1-44e9-af91-cf99b8ce8d98",
            "compositeImage": {
                "id": "5e16da4e-e594-4af7-9485-19ee53284841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74fced98-5ab3-4438-a74a-59d7b1e81efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a986b1-d385-436f-b49d-3d60f1d8022d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74fced98-5ab3-4438-a74a-59d7b1e81efc",
                    "LayerId": "890cd01d-7adf-4bb6-82da-5e4845d2113a"
                }
            ]
        },
        {
            "id": "ae01396d-51ee-4386-b2b0-ce3099a6eb2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49063b5-6fa1-44e9-af91-cf99b8ce8d98",
            "compositeImage": {
                "id": "9fc1232e-d252-4b0b-9d61-5bac6ecf068d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae01396d-51ee-4386-b2b0-ce3099a6eb2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a655c46-2774-4d0f-a30b-7de780b3b4b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae01396d-51ee-4386-b2b0-ce3099a6eb2b",
                    "LayerId": "890cd01d-7adf-4bb6-82da-5e4845d2113a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "890cd01d-7adf-4bb6-82da-5e4845d2113a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d49063b5-6fa1-44e9-af91-cf99b8ce8d98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 18,
    "yorig": 64
}