{
    "id": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCoin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e36a4850-4553-4599-8fbe-69be5aede2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
            "compositeImage": {
                "id": "ab6c9bbf-0c07-439a-9c3a-dc082a788560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36a4850-4553-4599-8fbe-69be5aede2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4311c521-e11b-4913-beab-0494d658fd07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36a4850-4553-4599-8fbe-69be5aede2f4",
                    "LayerId": "2d9e9cf4-e168-493a-b992-c4f311bc11aa"
                }
            ]
        },
        {
            "id": "66bb98ac-8e78-411f-b80a-83e7ba7631f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
            "compositeImage": {
                "id": "0c08fae1-70ff-45c5-84d8-b7d245da22fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66bb98ac-8e78-411f-b80a-83e7ba7631f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200c2dac-a190-4bab-a5df-1598bfae12eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66bb98ac-8e78-411f-b80a-83e7ba7631f9",
                    "LayerId": "2d9e9cf4-e168-493a-b992-c4f311bc11aa"
                }
            ]
        },
        {
            "id": "5f7dadd4-7aa3-4124-8e53-d01ef4290565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
            "compositeImage": {
                "id": "9577793f-beb6-4a32-85f7-a2981faa44b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7dadd4-7aa3-4124-8e53-d01ef4290565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cc50ff-c187-4b27-bc9c-e10f27bdddb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7dadd4-7aa3-4124-8e53-d01ef4290565",
                    "LayerId": "2d9e9cf4-e168-493a-b992-c4f311bc11aa"
                }
            ]
        },
        {
            "id": "bf0255b1-f98f-4710-8cae-c6f410fa7c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
            "compositeImage": {
                "id": "bf9fa653-3cde-4bc5-ae82-0b86dc6beb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0255b1-f98f-4710-8cae-c6f410fa7c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce8be7f-f22e-4a04-a7df-8233d59d6c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0255b1-f98f-4710-8cae-c6f410fa7c3d",
                    "LayerId": "2d9e9cf4-e168-493a-b992-c4f311bc11aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2d9e9cf4-e168-493a-b992-c4f311bc11aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 27
}