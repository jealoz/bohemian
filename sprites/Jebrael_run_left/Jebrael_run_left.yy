{
    "id": "95714a85-3476-4f61-9ad1-c9ab548832a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_run_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 749,
    "bbox_left": 296,
    "bbox_right": 606,
    "bbox_top": 676,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d29dc999-ceb7-428d-8fee-17de5015e6ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "2ccaacea-3257-4daf-9b04-98d7ec954cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29dc999-ceb7-428d-8fee-17de5015e6ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935370c1-ffda-4324-a3db-3665ae8440c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29dc999-ceb7-428d-8fee-17de5015e6ae",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "0c8ba36e-b1bc-4732-b356-945772721cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "57b0882f-b5c6-4f8f-a674-30021fb34c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8ba36e-b1bc-4732-b356-945772721cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ce4c57-df46-4175-901a-b3c759ae3894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8ba36e-b1bc-4732-b356-945772721cd7",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "a4cb650b-ed66-43c5-911a-5911fd83c14f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "690a8522-92a0-47c2-b1e9-7d11e824a248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4cb650b-ed66-43c5-911a-5911fd83c14f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06e3793-24f5-4eb4-9216-fdddba82a28d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4cb650b-ed66-43c5-911a-5911fd83c14f",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "ddd537b6-765e-44ac-9226-6f69b508af92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "b5c1535c-979d-4215-9f82-8ce7bfedc7fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddd537b6-765e-44ac-9226-6f69b508af92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9d5fba-91a4-45b0-9b69-d689e97c1ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddd537b6-765e-44ac-9226-6f69b508af92",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "4ca66216-b146-4ce8-82fe-d14facd800a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "b8cafdf7-5fc0-45fd-9275-736dfa28b397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ca66216-b146-4ce8-82fe-d14facd800a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d813e5f-afb2-4ea1-9048-1b992ee468cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ca66216-b146-4ce8-82fe-d14facd800a2",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "a4bf2750-2abe-435a-b956-ff9ff1ce36df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "e0ccda1c-b155-4cea-813f-913c854681d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4bf2750-2abe-435a-b956-ff9ff1ce36df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3566e538-b3b4-4853-af74-b421d28d4d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4bf2750-2abe-435a-b956-ff9ff1ce36df",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "22e8c1f8-383b-454a-9cf2-9c3fb71e2953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "b73804a0-4df6-4064-a278-2748b6e083ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e8c1f8-383b-454a-9cf2-9c3fb71e2953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c0c061-06e1-482d-a6ec-3abd56d50cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e8c1f8-383b-454a-9cf2-9c3fb71e2953",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "88cb1d3c-cb39-455c-9927-93947db0f08d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "44d2b4d6-8618-4b72-8c65-35c151a05d33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cb1d3c-cb39-455c-9927-93947db0f08d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d456ea42-8e75-41a9-aee3-328eeb32529a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cb1d3c-cb39-455c-9927-93947db0f08d",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "1ad55d59-c295-4e1d-b50a-e8472a052420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "db8c5e25-8801-4270-a518-67eb0edaa87d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ad55d59-c295-4e1d-b50a-e8472a052420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "272fa93b-232d-44ab-8bf4-af6858432ef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ad55d59-c295-4e1d-b50a-e8472a052420",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "ded28f2c-cb21-4b8c-83cf-32c261e8df97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "89f5f96f-7bf7-4d5c-8813-ecc99aab28f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded28f2c-cb21-4b8c-83cf-32c261e8df97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b68ac81-706e-4fd6-8057-1c8d79a60126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded28f2c-cb21-4b8c-83cf-32c261e8df97",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "d20a6d39-1280-419b-a1e1-c76766f42fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "3a9de218-e6e3-4cad-91c0-9c2d52d35b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d20a6d39-1280-419b-a1e1-c76766f42fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721d70c4-7da6-422f-9bce-80112a10ba1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d20a6d39-1280-419b-a1e1-c76766f42fdd",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        },
        {
            "id": "6c56c6db-4a10-4c8f-ab7c-b689aea039b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "compositeImage": {
                "id": "8c556be2-5c45-45ab-a64e-594586b8cb23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c56c6db-4a10-4c8f-ab7c-b689aea039b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cacb393b-e774-425e-9252-649593c14d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c56c6db-4a10-4c8f-ab7c-b689aea039b9",
                    "LayerId": "f2196005-962e-40f3-b96d-50518c3d58b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "f2196005-962e-40f3-b96d-50518c3d58b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95714a85-3476-4f61-9ad1-c9ab548832a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 434,
    "yorig": 588
}