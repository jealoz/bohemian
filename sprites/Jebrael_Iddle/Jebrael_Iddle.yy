{
    "id": "1b926432-229c-456d-afb2-05fca8de70ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_Iddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 750,
    "bbox_left": 300,
    "bbox_right": 601,
    "bbox_top": 704,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "116d891a-69e9-4e27-b78b-b4be86197c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "a8b2e886-3c99-457a-89d3-1498d402fa5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116d891a-69e9-4e27-b78b-b4be86197c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b0db134-f99e-46ac-a355-ee322232df36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116d891a-69e9-4e27-b78b-b4be86197c27",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "2b817da0-9931-4f1c-9ebe-ae113f9e1a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "3b7cd1dd-0150-44b7-9f50-902189ea449d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b817da0-9931-4f1c-9ebe-ae113f9e1a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d604336-05b9-400a-b9a5-9c0f9178eab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b817da0-9931-4f1c-9ebe-ae113f9e1a8b",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "8f824ca4-0b8a-433b-8117-e2a2c181c950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "09521807-ffec-4abb-b31f-28b02ede0374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f824ca4-0b8a-433b-8117-e2a2c181c950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04974b43-be21-42ac-9d05-67729f5516b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f824ca4-0b8a-433b-8117-e2a2c181c950",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "e71b39be-7768-4d4b-8a96-0a6b6a9a2368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "2b973491-1106-4d28-806a-4a3fe7d2d6bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71b39be-7768-4d4b-8a96-0a6b6a9a2368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928283de-f32d-4fe1-a9b8-4e54d86578e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71b39be-7768-4d4b-8a96-0a6b6a9a2368",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "82611c66-0c66-44ea-919b-347d7905a4b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "facbe3a1-142f-411d-9b8d-7d29cfaf599b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82611c66-0c66-44ea-919b-347d7905a4b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce13375c-2269-4867-8e7f-ff0ff9dd42e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82611c66-0c66-44ea-919b-347d7905a4b9",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "e40e5a96-957c-45da-9e9b-aa2205ebe961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "7b4bfdd4-c81c-4bea-9b1d-aacd7d3d4c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e40e5a96-957c-45da-9e9b-aa2205ebe961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45564f14-8b51-4296-878f-c2591fe68749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e40e5a96-957c-45da-9e9b-aa2205ebe961",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "edfa8dd4-8d17-471f-9c60-e0e544bebd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "28320815-f5e1-4262-ba39-bcdeee630a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edfa8dd4-8d17-471f-9c60-e0e544bebd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7042553-95b2-4d16-95af-cf45ac5b132d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edfa8dd4-8d17-471f-9c60-e0e544bebd84",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "7ab26082-6f86-4816-b131-8077e168809d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "b82c8e40-e93a-472d-b0c0-fe0ec836f3a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab26082-6f86-4816-b131-8077e168809d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c39585e-9ba3-4348-ae07-4567be2c55c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab26082-6f86-4816-b131-8077e168809d",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "b14a0cfa-3419-4455-8f70-e6deb9d29554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "de25c625-68f5-49b6-a416-346403473776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14a0cfa-3419-4455-8f70-e6deb9d29554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71192e1f-f1f4-4fd9-a104-4458d0e630db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14a0cfa-3419-4455-8f70-e6deb9d29554",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "92654570-cdbe-4d4a-a86d-d2aa5e2cacc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "81500dcf-153b-4aa2-89b7-e6ec7def1222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92654570-cdbe-4d4a-a86d-d2aa5e2cacc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "338e4f0e-0b31-4d84-89ad-e6f8e7736b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92654570-cdbe-4d4a-a86d-d2aa5e2cacc0",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "ab620cc1-01fb-4689-b26b-e7c19df6c1af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "76fef908-79a3-4bf3-93d1-b0b31757e362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab620cc1-01fb-4689-b26b-e7c19df6c1af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23de5b32-95aa-4654-9285-c572438852f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab620cc1-01fb-4689-b26b-e7c19df6c1af",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "9eabca36-439d-414e-9aa4-d8d34fa38115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "ef874d05-bcdf-409c-a881-2c96c4d6a8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eabca36-439d-414e-9aa4-d8d34fa38115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1179ee52-cef7-45c0-a358-b551e9cb269a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eabca36-439d-414e-9aa4-d8d34fa38115",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "e05e7f4b-c978-4545-867b-63c547345bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "498bcbd8-6147-405b-8e1f-36b28eb1a7be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05e7f4b-c978-4545-867b-63c547345bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da101bff-ac5c-4860-8a3f-d63dc7e5414b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05e7f4b-c978-4545-867b-63c547345bda",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "5e2424b7-0495-4441-ac24-4bc3f26f2a2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "d8daab61-969b-4b49-98d1-3aa7950c0a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2424b7-0495-4441-ac24-4bc3f26f2a2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3557653-5407-44b9-a1cf-a6bfe6fcaba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2424b7-0495-4441-ac24-4bc3f26f2a2b",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "f615f122-dc43-47c1-8515-9d7f54d7b1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "21440d10-fec0-418f-aae6-1bcd91208c91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f615f122-dc43-47c1-8515-9d7f54d7b1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "757715b2-b68b-4a1d-8a09-fb985fe00964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f615f122-dc43-47c1-8515-9d7f54d7b1eb",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "f15687dc-b8bf-472b-a5ad-2cbfb5b38232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "58dab13b-ea31-4c06-970c-ad5d4a03c803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15687dc-b8bf-472b-a5ad-2cbfb5b38232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d1580e-338e-462e-ae1b-1def392af405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15687dc-b8bf-472b-a5ad-2cbfb5b38232",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "bce847dc-9579-460f-9535-c2c3a2dab1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "4ad0f3ce-a6f4-4402-909d-8ba1a09245ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce847dc-9579-460f-9535-c2c3a2dab1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41642030-795a-435a-8d58-4861c2f3a212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce847dc-9579-460f-9535-c2c3a2dab1ea",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        },
        {
            "id": "7486b0fa-1917-4279-ab1e-d2c1a2ddd1a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "compositeImage": {
                "id": "38129bad-4035-47c8-82ea-fd42749ad813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7486b0fa-1917-4279-ab1e-d2c1a2ddd1a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c003a11-8d19-477b-bf45-1f8c531d756e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7486b0fa-1917-4279-ab1e-d2c1a2ddd1a1",
                    "LayerId": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "a6eaeb55-18b9-4ed8-a30b-7c1b01e6220e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "61419abb-02ed-420e-85b7-f77b76707ccb",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}