{
    "id": "60d73ffe-7908-4482-b3ba-7a6f9c0e74d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 4,
    "bbox_right": 63,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e0721bb-359c-4960-b204-60bde09c415f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d73ffe-7908-4482-b3ba-7a6f9c0e74d6",
            "compositeImage": {
                "id": "fb123f5c-6854-431d-b16c-5a4d5dc3ba5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0721bb-359c-4960-b204-60bde09c415f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a308e8c7-dce8-464c-9d25-f43829f58918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0721bb-359c-4960-b204-60bde09c415f",
                    "LayerId": "33003782-448c-452a-984e-791859ef42ab"
                }
            ]
        },
        {
            "id": "7914b88a-5ce6-48da-bbfe-7cce3106f4b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d73ffe-7908-4482-b3ba-7a6f9c0e74d6",
            "compositeImage": {
                "id": "0d2a3c34-795e-41a9-89b9-66ffbef1db38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7914b88a-5ce6-48da-bbfe-7cce3106f4b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a739628-d4b2-4010-9547-1ed2c3e70e10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7914b88a-5ce6-48da-bbfe-7cce3106f4b1",
                    "LayerId": "33003782-448c-452a-984e-791859ef42ab"
                }
            ]
        },
        {
            "id": "929cbcf8-10ae-4e37-897a-3fa55260a0f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d73ffe-7908-4482-b3ba-7a6f9c0e74d6",
            "compositeImage": {
                "id": "937cff4e-0fa8-4070-a94a-555b35646959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929cbcf8-10ae-4e37-897a-3fa55260a0f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e181e07-c6fd-4a9e-bb34-8160e0fb15b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929cbcf8-10ae-4e37-897a-3fa55260a0f9",
                    "LayerId": "33003782-448c-452a-984e-791859ef42ab"
                }
            ]
        },
        {
            "id": "434552f4-05ae-4a11-9d4e-8810c2064ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d73ffe-7908-4482-b3ba-7a6f9c0e74d6",
            "compositeImage": {
                "id": "865ff6b5-f097-4e2d-8510-ca5d357805b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "434552f4-05ae-4a11-9d4e-8810c2064ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e76562e5-7597-4ecf-93d7-45fb3b817734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "434552f4-05ae-4a11-9d4e-8810c2064ed1",
                    "LayerId": "33003782-448c-452a-984e-791859ef42ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "33003782-448c-452a-984e-791859ef42ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60d73ffe-7908-4482-b3ba-7a6f9c0e74d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 41,
    "yorig": 60
}