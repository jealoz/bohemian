{
    "id": "accb3697-ed2d-4b60-a63d-6de53729a597",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "474c9234-462a-4e36-9b99-a2401cb1300e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "accb3697-ed2d-4b60-a63d-6de53729a597",
            "compositeImage": {
                "id": "e7a998b5-c924-44a7-9049-690feee83181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "474c9234-462a-4e36-9b99-a2401cb1300e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24ab5a8-4581-4b9a-bc66-79cec791be30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "474c9234-462a-4e36-9b99-a2401cb1300e",
                    "LayerId": "cade7668-7f9c-4a15-94cd-3189c10c8a49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cade7668-7f9c-4a15-94cd-3189c10c8a49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "accb3697-ed2d-4b60-a63d-6de53729a597",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 29
}