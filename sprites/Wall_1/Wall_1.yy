{
    "id": "2876f835-b36a-4213-b258-ac26452c294c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Wall_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c727b18-f7cb-459c-ad5d-b58b602dbfc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2876f835-b36a-4213-b258-ac26452c294c",
            "compositeImage": {
                "id": "1897ffff-8ef7-4df5-8a7c-04a886d282c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c727b18-f7cb-459c-ad5d-b58b602dbfc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc13ebd0-f757-41a4-bc2b-d92de03b3122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c727b18-f7cb-459c-ad5d-b58b602dbfc4",
                    "LayerId": "424ee542-b5c2-48e0-88c9-3f54c75f5208"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "424ee542-b5c2-48e0-88c9-3f54c75f5208",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2876f835-b36a-4213-b258-ac26452c294c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}