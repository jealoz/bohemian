{
    "id": "3dc41c6e-fff3-4064-9554-f3a84c01e5c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wallpaper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d9513be-cbf3-4276-a63b-98834d0f642b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dc41c6e-fff3-4064-9554-f3a84c01e5c0",
            "compositeImage": {
                "id": "c5c4aeea-bbad-485f-8267-f323f4c656e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d9513be-cbf3-4276-a63b-98834d0f642b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059a60bf-64f8-4bbb-91fd-adb0ff08d9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d9513be-cbf3-4276-a63b-98834d0f642b",
                    "LayerId": "3a1f1ea4-1f37-4668-986e-597027b1c0aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "3a1f1ea4-1f37-4668-986e-597027b1c0aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dc41c6e-fff3-4064-9554-f3a84c01e5c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}