{
    "id": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jealoz_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 730,
    "bbox_left": 373,
    "bbox_right": 514,
    "bbox_top": 155,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46b79f85-4e17-4c5c-a644-f11dad8f44cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "compositeImage": {
                "id": "493ff454-51a3-4233-90bd-216b0ab103f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b79f85-4e17-4c5c-a644-f11dad8f44cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19aa7184-01ff-4f13-a5ee-9f7d85cc70c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b79f85-4e17-4c5c-a644-f11dad8f44cf",
                    "LayerId": "98a53517-caea-4179-8172-62fe16155b0d"
                }
            ]
        },
        {
            "id": "b0c23ce7-105b-44ac-b319-58c288200c42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "compositeImage": {
                "id": "666b09d0-9736-45c3-94c5-112b313192b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0c23ce7-105b-44ac-b319-58c288200c42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a8e68cd-927a-497b-bb57-3ef46b66e1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0c23ce7-105b-44ac-b319-58c288200c42",
                    "LayerId": "98a53517-caea-4179-8172-62fe16155b0d"
                }
            ]
        },
        {
            "id": "0d3463cd-5a64-4650-a3ed-524e7d4d4630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "compositeImage": {
                "id": "56dec0ec-730b-4c78-827e-8e469e700054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d3463cd-5a64-4650-a3ed-524e7d4d4630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df72f20a-b338-4fec-82ce-eae6e5cb9a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d3463cd-5a64-4650-a3ed-524e7d4d4630",
                    "LayerId": "98a53517-caea-4179-8172-62fe16155b0d"
                }
            ]
        },
        {
            "id": "199554ec-7352-443c-8a75-392856df6036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "compositeImage": {
                "id": "7c154de8-0c4c-4741-a17a-ccfa0895cd7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199554ec-7352-443c-8a75-392856df6036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62ccdf8-8b3a-493b-8f5d-5243b85de776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199554ec-7352-443c-8a75-392856df6036",
                    "LayerId": "98a53517-caea-4179-8172-62fe16155b0d"
                }
            ]
        },
        {
            "id": "18a49c26-2ac7-4534-bb6e-5e24950ea5cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "compositeImage": {
                "id": "dde48801-f153-45ef-906b-b4e0183e14a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18a49c26-2ac7-4534-bb6e-5e24950ea5cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78290c7f-a5ae-4684-84b0-eaf1ad4bfbf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18a49c26-2ac7-4534-bb6e-5e24950ea5cb",
                    "LayerId": "98a53517-caea-4179-8172-62fe16155b0d"
                }
            ]
        },
        {
            "id": "fb635187-fdf4-48a4-893e-c56e145c5094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "compositeImage": {
                "id": "104bdbe5-c580-4d28-a184-39b59354ae7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb635187-fdf4-48a4-893e-c56e145c5094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d97c4fae-86fd-4faf-9116-0beaab0aeae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb635187-fdf4-48a4-893e-c56e145c5094",
                    "LayerId": "98a53517-caea-4179-8172-62fe16155b0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "98a53517-caea-4179-8172-62fe16155b0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38398f43-7fcb-4ff2-813d-cb8d48658c3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 450
}