{
    "id": "31da1e63-6ea1-46fa-8aab-498890d86643",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8acb459-e561-4666-9db2-dbc94d8648ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31da1e63-6ea1-46fa-8aab-498890d86643",
            "compositeImage": {
                "id": "ce4cb9cc-3f8f-4035-ab53-4ad4ab7a51c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8acb459-e561-4666-9db2-dbc94d8648ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5515475-ed7e-49a8-8375-7ad31e3f6b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8acb459-e561-4666-9db2-dbc94d8648ba",
                    "LayerId": "1187b379-f39d-4661-922e-75e22ba40997"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "1187b379-f39d-4661-922e-75e22ba40997",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31da1e63-6ea1-46fa-8aab-498890d86643",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}