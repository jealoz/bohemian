{
    "id": "68079e4e-079b-4f10-b2de-2435ec482fe1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rocas1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "853007cc-1b9f-41d3-b11b-eba55d49d465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68079e4e-079b-4f10-b2de-2435ec482fe1",
            "compositeImage": {
                "id": "596200f3-811b-4817-86c1-5c4f3923d692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853007cc-1b9f-41d3-b11b-eba55d49d465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca4d19db-fa1e-4443-a403-50ab562a1384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853007cc-1b9f-41d3-b11b-eba55d49d465",
                    "LayerId": "75a1420c-d2c4-4c66-a26a-2b25c4bb5ac0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "75a1420c-d2c4-4c66-a26a-2b25c4bb5ac0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68079e4e-079b-4f10-b2de-2435ec482fe1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 46,
    "yorig": 33
}