{
    "id": "482efcd0-8a93-45f5-a1b7-f359b3c579f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Textos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2a2156d-27ae-4fb4-a338-f7c689b89786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "482efcd0-8a93-45f5-a1b7-f359b3c579f7",
            "compositeImage": {
                "id": "87635812-7b34-41e6-b950-9be5adc1c208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2a2156d-27ae-4fb4-a338-f7c689b89786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde6f6d8-5220-44c1-ba02-be6ab97cdbae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2a2156d-27ae-4fb4-a338-f7c689b89786",
                    "LayerId": "b8d856d2-b6a2-44fe-82ec-4c9d932f2bbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8d856d2-b6a2-44fe-82ec-4c9d932f2bbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "482efcd0-8a93-45f5-a1b7-f359b3c579f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}