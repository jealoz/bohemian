{
    "id": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "AndinoJames",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 485,
    "bbox_left": 0,
    "bbox_right": 317,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "891e9609-e16f-4c59-b961-1c4307c49f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "8e563aa9-e60e-45a6-b70f-2e6b9440d85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "891e9609-e16f-4c59-b961-1c4307c49f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a906bba-0ae7-4761-b043-cb62fdc0d19f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "891e9609-e16f-4c59-b961-1c4307c49f33",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "8cbdf735-99a3-4fea-bd4e-f1f349b3f375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "a170c803-95ff-4ebd-885d-9c8d468387e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbdf735-99a3-4fea-bd4e-f1f349b3f375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779edd65-cc4e-49d1-9a79-929fa83b2203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbdf735-99a3-4fea-bd4e-f1f349b3f375",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "1788c106-1fbb-47d1-980e-966ce794dc65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "7057f147-d7e2-4f38-92e7-8f15ce6dc35d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1788c106-1fbb-47d1-980e-966ce794dc65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58709615-b7fd-4b4e-a4b3-7ba0cf272481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1788c106-1fbb-47d1-980e-966ce794dc65",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "35c62b5e-b0cc-4be1-8b9f-0746662edaca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "afdfe1ff-50ce-4d2d-a518-f9aa5cff49ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35c62b5e-b0cc-4be1-8b9f-0746662edaca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2aa2202-c6de-4f99-9153-7d09b89fa437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35c62b5e-b0cc-4be1-8b9f-0746662edaca",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "bc714d07-cc4d-46f2-9abf-5964866c64f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "00e92c12-40f6-40b2-b46d-67e21221ef53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc714d07-cc4d-46f2-9abf-5964866c64f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d272dc42-93cd-4ad6-b3c0-b6769f41b346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc714d07-cc4d-46f2-9abf-5964866c64f5",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "48a12381-d43b-49c4-97f4-f74a09cd4180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "81404b80-3305-48b9-86b8-2edb0ccd027c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a12381-d43b-49c4-97f4-f74a09cd4180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3598edc8-1611-495f-a5cf-9658440d4e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a12381-d43b-49c4-97f4-f74a09cd4180",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "6d801326-d842-49aa-97fb-fa5df808375b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "bf78c700-7617-41d1-b9ad-f919f923b317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d801326-d842-49aa-97fb-fa5df808375b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e7fa5a-c61e-4349-82da-e3979f198bb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d801326-d842-49aa-97fb-fa5df808375b",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "ca6be3f9-ba6d-4fcc-8b02-159b91ae5e8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "88dec6f0-1763-4cf3-9175-05fcfddba683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6be3f9-ba6d-4fcc-8b02-159b91ae5e8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e708902-3ab9-40e0-8965-de9a2531d420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6be3f9-ba6d-4fcc-8b02-159b91ae5e8c",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "d57dc764-79e3-4b66-b98b-5fb046ef41e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "e28cec5a-b591-4040-a3cc-7ace6969a6ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57dc764-79e3-4b66-b98b-5fb046ef41e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34c7765f-6272-4e2f-86bf-5ef7b296a6b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57dc764-79e3-4b66-b98b-5fb046ef41e9",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        },
        {
            "id": "691398ca-f72d-4b9f-a9c9-f83e79dfc7a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "compositeImage": {
                "id": "8905c62a-47fb-4b1e-9a10-f9b14058297d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691398ca-f72d-4b9f-a9c9-f83e79dfc7a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f76acf1-e310-4a5b-8732-3178929228db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691398ca-f72d-4b9f-a9c9-f83e79dfc7a1",
                    "LayerId": "a8368c8a-72cc-410e-8986-8d2e085c16ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 486,
    "layers": [
        {
            "id": "a8368c8a-72cc-410e-8986-8d2e085c16ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f84466f-7606-4c06-9d75-7ae11a8b5475",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 319,
    "xorig": 0,
    "yorig": 0
}