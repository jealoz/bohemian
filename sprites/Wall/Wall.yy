{
    "id": "d44a678b-bf18-414e-a9b7-6bef88147d6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 2,
    "bbox_right": 742,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d85a839c-3035-4004-8e0d-eb0a9ec62efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44a678b-bf18-414e-a9b7-6bef88147d6f",
            "compositeImage": {
                "id": "1cbf4f2e-7f62-4bff-b2b7-7c27b600f768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d85a839c-3035-4004-8e0d-eb0a9ec62efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6a24a3-77ee-4dad-9fd2-082b19ea5c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d85a839c-3035-4004-8e0d-eb0a9ec62efb",
                    "LayerId": "7c38bdcd-18a9-4e31-9c1b-1ebf2552a503"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 231,
    "layers": [
        {
            "id": "7c38bdcd-18a9-4e31-9c1b-1ebf2552a503",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d44a678b-bf18-414e-a9b7-6bef88147d6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 839,
    "xorig": 0,
    "yorig": 0
}