{
    "id": "66a0d446-fbee-4c74-bbb6-2fb3f2be110b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJumpUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 4,
    "bbox_right": 101,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "455e7cad-312d-44b4-ab5f-75f209fced13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a0d446-fbee-4c74-bbb6-2fb3f2be110b",
            "compositeImage": {
                "id": "493d5606-347d-4215-ae5f-e0d4d719b3ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455e7cad-312d-44b4-ab5f-75f209fced13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63eeec3e-ba84-45f6-a74f-fa039f2326e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455e7cad-312d-44b4-ab5f-75f209fced13",
                    "LayerId": "19bc83a0-56a2-45c7-93e3-abbf4decdef9"
                }
            ]
        },
        {
            "id": "8edc5753-6abd-4816-a782-e52f088a3d02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a0d446-fbee-4c74-bbb6-2fb3f2be110b",
            "compositeImage": {
                "id": "bbe7ca4d-a49a-4b51-aa1b-1f3d4483bccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edc5753-6abd-4816-a782-e52f088a3d02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56146674-ad80-4b17-84af-6014e77df8e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edc5753-6abd-4816-a782-e52f088a3d02",
                    "LayerId": "19bc83a0-56a2-45c7-93e3-abbf4decdef9"
                }
            ]
        },
        {
            "id": "62c18962-94d7-4781-9793-80fcc9a49352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a0d446-fbee-4c74-bbb6-2fb3f2be110b",
            "compositeImage": {
                "id": "457fa9e7-8ada-47ac-955d-c0671cf35dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62c18962-94d7-4781-9793-80fcc9a49352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c90026-bb2b-4802-9d66-b8057f86e35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62c18962-94d7-4781-9793-80fcc9a49352",
                    "LayerId": "19bc83a0-56a2-45c7-93e3-abbf4decdef9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "19bc83a0-56a2-45c7-93e3-abbf4decdef9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66a0d446-fbee-4c74-bbb6-2fb3f2be110b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 58,
    "yorig": 96
}