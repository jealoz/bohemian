{
    "id": "7696417f-081c-4163-8ecc-df427f5655e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 3,
    "bbox_right": 43,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34fb75d6-1a29-414d-a2b7-0050a911f342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7696417f-081c-4163-8ecc-df427f5655e8",
            "compositeImage": {
                "id": "6ca1e2c3-1ed9-47a5-9826-b8f330ab39c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34fb75d6-1a29-414d-a2b7-0050a911f342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87241a5c-0d4e-43dd-9f15-55c99ebacd89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34fb75d6-1a29-414d-a2b7-0050a911f342",
                    "LayerId": "7c3d3592-27e8-44dc-8828-e0e7d984556e"
                }
            ]
        },
        {
            "id": "2f7d5447-c137-4b89-b422-93f1ec855454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7696417f-081c-4163-8ecc-df427f5655e8",
            "compositeImage": {
                "id": "e7f6c256-4cf5-4a5a-84a4-184c75f8782b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7d5447-c137-4b89-b422-93f1ec855454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2eaee4e-2bca-4cca-bdc9-04c7fa3c6616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7d5447-c137-4b89-b422-93f1ec855454",
                    "LayerId": "7c3d3592-27e8-44dc-8828-e0e7d984556e"
                }
            ]
        },
        {
            "id": "e1b4d0b9-4140-4375-ade4-d3abc07e64f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7696417f-081c-4163-8ecc-df427f5655e8",
            "compositeImage": {
                "id": "66d6f8a1-59f9-4895-8979-86acc6873734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b4d0b9-4140-4375-ade4-d3abc07e64f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f51d4968-3679-4436-84cb-d511fd93a90b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b4d0b9-4140-4375-ade4-d3abc07e64f6",
                    "LayerId": "7c3d3592-27e8-44dc-8828-e0e7d984556e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7c3d3592-27e8-44dc-8828-e0e7d984556e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7696417f-081c-4163-8ecc-df427f5655e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 22,
    "yorig": 40
}