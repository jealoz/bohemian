{
    "id": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 750,
    "bbox_left": 299,
    "bbox_right": 589,
    "bbox_top": 703,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11bc2316-e1a4-431f-8a10-9213f1a761bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "fb6d6927-1da1-4a50-bc0d-202e1eeb05bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11bc2316-e1a4-431f-8a10-9213f1a761bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50e52423-ebcc-4314-b88c-549dbba32415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11bc2316-e1a4-431f-8a10-9213f1a761bb",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "853c7d4c-731c-4f96-b67f-f3e26ce0c09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "83bfacb7-d2a7-4c1f-a85c-648f5646282b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853c7d4c-731c-4f96-b67f-f3e26ce0c09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed61f06e-167d-478e-8460-3c43c05d011a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853c7d4c-731c-4f96-b67f-f3e26ce0c09d",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "522864d7-812c-4b3d-aff3-9aca0eaeaedc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "912429f4-feb0-4cc9-8677-701eeb252a7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522864d7-812c-4b3d-aff3-9aca0eaeaedc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b352a1-63e0-4756-bf5e-528de2dba358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522864d7-812c-4b3d-aff3-9aca0eaeaedc",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "d1f670fc-91e7-4b43-86b6-a215c6b5e14f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "78bc8de2-68e0-4ab8-bfed-fc630015ba6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f670fc-91e7-4b43-86b6-a215c6b5e14f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e327e3b-c9fa-4479-9ae4-65fe107356da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f670fc-91e7-4b43-86b6-a215c6b5e14f",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "e77d84b4-9245-45ec-926b-22139b389afd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "6f6996dc-dd7a-4d6c-8925-0c202eeb1a69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e77d84b4-9245-45ec-926b-22139b389afd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b725fc2f-6aad-4c33-9325-c694557db1e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e77d84b4-9245-45ec-926b-22139b389afd",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "3c1ff273-78c8-483b-a2ae-406a1ca4ca9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "b02e4871-3fe0-49f9-9e59-0fbd42fc436f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1ff273-78c8-483b-a2ae-406a1ca4ca9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53340793-59df-480a-8d3c-c41fc1f6b297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1ff273-78c8-483b-a2ae-406a1ca4ca9c",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "6cd33de2-6e50-4a97-a371-a19e6c1d1533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "6ef32975-9a19-4985-9859-c55fdf1b4a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd33de2-6e50-4a97-a371-a19e6c1d1533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe25b51-892f-4d1e-bb79-6a51d66684d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd33de2-6e50-4a97-a371-a19e6c1d1533",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "f32c23ae-d304-4edc-b895-95f52cd3189e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "f4d94048-19d5-45d8-87e6-440622275e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f32c23ae-d304-4edc-b895-95f52cd3189e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f448628-5302-4ebe-8c03-522481d27821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f32c23ae-d304-4edc-b895-95f52cd3189e",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "2c72a700-e44e-49b4-b15c-64ffba68c0c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "9c603ad7-69e7-4e7d-98eb-3d018078d6f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c72a700-e44e-49b4-b15c-64ffba68c0c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59356603-5704-447f-b1fa-8eda00f93cfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c72a700-e44e-49b4-b15c-64ffba68c0c5",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "89b4d164-ffc6-44f7-acaa-e277ff5e742d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "98ec5ab5-f938-4135-98c0-36b700aa5602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b4d164-ffc6-44f7-acaa-e277ff5e742d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c049f6-25c7-47e6-ad12-91ba2179d8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b4d164-ffc6-44f7-acaa-e277ff5e742d",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "cd101b43-ca49-4f4c-8d1b-7bba9974985b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "a2aed5ba-198b-46ca-b3a9-39540bf39a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd101b43-ca49-4f4c-8d1b-7bba9974985b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5eb7d69-fdc0-49e5-8500-b742173bb241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd101b43-ca49-4f4c-8d1b-7bba9974985b",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "89edf9d8-c133-4493-8471-25dd91304899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "cee90eb6-c696-4927-bbde-7385fd4836a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89edf9d8-c133-4493-8471-25dd91304899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb87b3eb-0074-4102-be81-8fc28af05158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89edf9d8-c133-4493-8471-25dd91304899",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "e4a09da3-9094-49aa-bcaa-44ecf258a9c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "19c2a2a8-a2b6-490a-a268-804cc450057f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a09da3-9094-49aa-bcaa-44ecf258a9c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cabbfa4-2333-4239-82f0-a6a7f1a636d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a09da3-9094-49aa-bcaa-44ecf258a9c6",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "12d39cd1-18ea-4262-949e-506fac381e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "915e9bf4-e95f-4b48-b415-5b8e53c4243f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d39cd1-18ea-4262-949e-506fac381e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd9a08d-388f-4650-ac96-b06a7b8a6b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d39cd1-18ea-4262-949e-506fac381e8a",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "9ec5d5e2-2ee6-4d9a-8e1b-ce737a7a6c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "f40dee30-71a0-4731-a4b3-712ffcec5591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec5d5e2-2ee6-4d9a-8e1b-ce737a7a6c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d584cd-af44-41a0-83c1-69079fb184bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec5d5e2-2ee6-4d9a-8e1b-ce737a7a6c5b",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "fe57db31-3d72-47af-b0f2-80358d951241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "6190cd12-8130-44ab-9b44-c4e71a72cb77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe57db31-3d72-47af-b0f2-80358d951241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc874ce-37ca-444c-ad33-555dd20f132a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe57db31-3d72-47af-b0f2-80358d951241",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "8bfc7510-a37c-4eca-a789-3dc139eb9805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "14d021e0-928c-4b84-b05f-e8764c2085a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bfc7510-a37c-4eca-a789-3dc139eb9805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b91831-372a-44b4-a5a4-45a728b02000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bfc7510-a37c-4eca-a789-3dc139eb9805",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        },
        {
            "id": "ea6d36e5-aa11-45b1-b325-36afff9b4cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "compositeImage": {
                "id": "168064d0-65cf-41ee-9a62-4cc31254294b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6d36e5-aa11-45b1-b325-36afff9b4cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cd7d445-0d9d-4091-b9be-da36f7dd88e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6d36e5-aa11-45b1-b325-36afff9b4cdd",
                    "LayerId": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "b6b1fb3c-b63e-4d7c-85f5-0868abeaede9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cc01dde-8c78-429a-8bf5-5fcac740bfd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": -729,
    "yorig": 1113
}