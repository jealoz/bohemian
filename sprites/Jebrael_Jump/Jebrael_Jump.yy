{
    "id": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_Jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 750,
    "bbox_left": 172,
    "bbox_right": 659,
    "bbox_top": 136,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "560148ac-5887-465e-b037-dc9a318142fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "compositeImage": {
                "id": "4288735c-3927-4770-b1f9-92b431ed1a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560148ac-5887-465e-b037-dc9a318142fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22385795-888b-410b-933f-b014bce4c5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560148ac-5887-465e-b037-dc9a318142fa",
                    "LayerId": "8c4c2500-8cb0-4e6b-a151-7441325b416e"
                }
            ]
        },
        {
            "id": "2b08b6e6-0d4a-4581-a89e-5e59b83ebf51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "compositeImage": {
                "id": "c565b47d-7c8d-4dbf-9779-a184db810ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b08b6e6-0d4a-4581-a89e-5e59b83ebf51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2fd2d19-39dc-4bef-9905-9402ead3b506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b08b6e6-0d4a-4581-a89e-5e59b83ebf51",
                    "LayerId": "8c4c2500-8cb0-4e6b-a151-7441325b416e"
                }
            ]
        },
        {
            "id": "ba2ed8fb-c50f-4641-a70e-0bc62925bd05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "compositeImage": {
                "id": "f6414799-be30-4758-a58e-66119c074670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2ed8fb-c50f-4641-a70e-0bc62925bd05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c37a0f5-d88a-4af5-afac-217c31ace0a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2ed8fb-c50f-4641-a70e-0bc62925bd05",
                    "LayerId": "8c4c2500-8cb0-4e6b-a151-7441325b416e"
                }
            ]
        },
        {
            "id": "3c9b3ccf-c4d3-4f57-b110-7dd5c91b0a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "compositeImage": {
                "id": "261d127c-024a-47b9-8128-8ba0036614b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c9b3ccf-c4d3-4f57-b110-7dd5c91b0a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9225ce2b-e620-44e6-8a6c-a77237cd7fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c9b3ccf-c4d3-4f57-b110-7dd5c91b0a12",
                    "LayerId": "8c4c2500-8cb0-4e6b-a151-7441325b416e"
                }
            ]
        },
        {
            "id": "63198b00-866a-4e4f-8cce-f575c64cdc53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "compositeImage": {
                "id": "9a59c05f-002a-4b50-8da6-9df67034b6d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63198b00-866a-4e4f-8cce-f575c64cdc53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e16f60bf-4a6e-4fef-b6d4-166fdfd6d08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63198b00-866a-4e4f-8cce-f575c64cdc53",
                    "LayerId": "8c4c2500-8cb0-4e6b-a151-7441325b416e"
                }
            ]
        },
        {
            "id": "9acabf0d-202b-4abe-8d27-521bfae2b0b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "compositeImage": {
                "id": "e3cfbd02-b1c8-4000-b160-9c01c9e129d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9acabf0d-202b-4abe-8d27-521bfae2b0b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da506050-3880-4167-90d2-4f3165e54542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9acabf0d-202b-4abe-8d27-521bfae2b0b6",
                    "LayerId": "8c4c2500-8cb0-4e6b-a151-7441325b416e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "8c4c2500-8cb0-4e6b-a151-7441325b416e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8867dd89-14c8-48ca-bf2f-1b4d5095d676",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}