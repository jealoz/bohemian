{
    "id": "62f527c8-ebbd-4f9e-aca1-affb1cd17695",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDefense",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 2,
    "bbox_right": 41,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45d00361-544b-4f54-8121-2ba2e399dfbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62f527c8-ebbd-4f9e-aca1-affb1cd17695",
            "compositeImage": {
                "id": "cade0698-04c4-4225-8f4a-2aa3395f91e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d00361-544b-4f54-8121-2ba2e399dfbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b068be-c1c6-4035-b657-29c75995d52d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d00361-544b-4f54-8121-2ba2e399dfbc",
                    "LayerId": "073d0de0-b175-4aed-9608-9ef108a2f3ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "073d0de0-b175-4aed-9608-9ef108a2f3ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62f527c8-ebbd-4f9e-aca1-affb1cd17695",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 87
}