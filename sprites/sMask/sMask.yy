{
    "id": "c8f378c0-3454-4804-a7ec-5c890196efd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9985516c-b85d-4da6-9728-11bbe6cb57e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8f378c0-3454-4804-a7ec-5c890196efd9",
            "compositeImage": {
                "id": "66865d9d-a472-46d3-837d-751f35dfb4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9985516c-b85d-4da6-9728-11bbe6cb57e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70f6b4d-f18d-4fbd-bdfb-6edc4ae1e520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9985516c-b85d-4da6-9728-11bbe6cb57e0",
                    "LayerId": "1023cea2-1937-4eff-9141-520fa4059ef9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1023cea2-1937-4eff-9141-520fa4059ef9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8f378c0-3454-4804-a7ec-5c890196efd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 96
}