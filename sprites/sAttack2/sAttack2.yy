{
    "id": "f9c37657-81fa-48a3-9b71-b00477698cf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAttack2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 3,
    "bbox_right": 98,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ce98c5e-4a5a-4112-9ed5-db31ba64a218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9c37657-81fa-48a3-9b71-b00477698cf3",
            "compositeImage": {
                "id": "260fedef-ab44-4d65-b3c7-e849609ab0fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ce98c5e-4a5a-4112-9ed5-db31ba64a218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb796cae-ffd4-4cd7-bdcd-11b117d995e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ce98c5e-4a5a-4112-9ed5-db31ba64a218",
                    "LayerId": "ba805695-cbac-44b3-8108-24589eb92ae2"
                }
            ]
        },
        {
            "id": "99c352c5-c645-4647-8395-7efa2df97d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9c37657-81fa-48a3-9b71-b00477698cf3",
            "compositeImage": {
                "id": "e81f70df-2ff1-4193-882c-0bf5c7a987b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c352c5-c645-4647-8395-7efa2df97d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0b6830a-287c-4f4c-b0c6-bc93ae645e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c352c5-c645-4647-8395-7efa2df97d3e",
                    "LayerId": "ba805695-cbac-44b3-8108-24589eb92ae2"
                }
            ]
        },
        {
            "id": "a1fb88de-c5f7-4bbc-b610-a87aa3529378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9c37657-81fa-48a3-9b71-b00477698cf3",
            "compositeImage": {
                "id": "d2e7efcc-deb4-4d35-8c4c-9950a4133d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1fb88de-c5f7-4bbc-b610-a87aa3529378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad516bdc-a0d8-410b-812d-c4a350b42729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1fb88de-c5f7-4bbc-b610-a87aa3529378",
                    "LayerId": "ba805695-cbac-44b3-8108-24589eb92ae2"
                }
            ]
        },
        {
            "id": "44da032c-7a63-4f0d-aed2-d5d46fbc6a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9c37657-81fa-48a3-9b71-b00477698cf3",
            "compositeImage": {
                "id": "37b2b32f-4d4d-4d0c-b6e5-caf9abfdda6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44da032c-7a63-4f0d-aed2-d5d46fbc6a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "588e82ee-dc43-492e-a306-31beada47c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44da032c-7a63-4f0d-aed2-d5d46fbc6a7b",
                    "LayerId": "ba805695-cbac-44b3-8108-24589eb92ae2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "ba805695-cbac-44b3-8108-24589eb92ae2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9c37657-81fa-48a3-9b71-b00477698cf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 52,
    "yorig": 77
}