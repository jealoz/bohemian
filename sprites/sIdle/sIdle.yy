{
    "id": "0402c2dc-ae77-4a67-9002-1a7d45cc29c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 4,
    "bbox_right": 36,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f36e839-0bbd-407f-a745-d55e9100fe39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0402c2dc-ae77-4a67-9002-1a7d45cc29c0",
            "compositeImage": {
                "id": "f421b94d-79a8-43f8-9c9b-05b2b7f46146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f36e839-0bbd-407f-a745-d55e9100fe39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14fe3ed6-65c6-412c-bfdd-f0bffb270a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f36e839-0bbd-407f-a745-d55e9100fe39",
                    "LayerId": "ba056b47-8f76-4d97-b9ea-ea94b8e7bfea"
                }
            ]
        },
        {
            "id": "7fda6436-4f73-4203-91c0-1ea1dfe9cfaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0402c2dc-ae77-4a67-9002-1a7d45cc29c0",
            "compositeImage": {
                "id": "7b8b6140-fdde-4cd4-8514-5be527a84649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fda6436-4f73-4203-91c0-1ea1dfe9cfaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52ddf42c-1677-4731-b988-8e892ece4f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fda6436-4f73-4203-91c0-1ea1dfe9cfaa",
                    "LayerId": "ba056b47-8f76-4d97-b9ea-ea94b8e7bfea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 99,
    "layers": [
        {
            "id": "ba056b47-8f76-4d97-b9ea-ea94b8e7bfea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0402c2dc-ae77-4a67-9002-1a7d45cc29c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 22,
    "yorig": 97
}