{
    "id": "3dc785dc-1e4b-400e-8e4f-1d201b67b406",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rocas4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 15,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d21425-b0ff-4aa8-a6e9-763d7523c0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dc785dc-1e4b-400e-8e4f-1d201b67b406",
            "compositeImage": {
                "id": "32cb3972-79d7-42d8-936c-ee27a28cb738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d21425-b0ff-4aa8-a6e9-763d7523c0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b164a2e9-b61b-4c7d-8a04-66e94bdd1c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d21425-b0ff-4aa8-a6e9-763d7523c0f5",
                    "LayerId": "ccab4d12-c37c-42b7-9ce4-68759b7d14c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ccab4d12-c37c-42b7-9ce4-68759b7d14c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dc785dc-1e4b-400e-8e4f-1d201b67b406",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 48,
    "yorig": 17
}