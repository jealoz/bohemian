{
    "id": "6d9d3b9c-421f-4ea7-b3ed-647c5b0c3fc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rocas5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7f5a677-5587-4a12-b7fc-c36f7dec8177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d9d3b9c-421f-4ea7-b3ed-647c5b0c3fc0",
            "compositeImage": {
                "id": "54a807b8-e168-417b-8746-12ae29346c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7f5a677-5587-4a12-b7fc-c36f7dec8177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c51d31-c1ac-4a05-bfd5-ced96fd4557c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7f5a677-5587-4a12-b7fc-c36f7dec8177",
                    "LayerId": "4558430b-5b2c-4dc4-abde-8f605402f452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4558430b-5b2c-4dc4-abde-8f605402f452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d9d3b9c-421f-4ea7-b3ed-647c5b0c3fc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 17,
    "yorig": 15
}