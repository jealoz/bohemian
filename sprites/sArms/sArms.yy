{
    "id": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArms",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0cbdb25-77a5-4c22-9f7d-88ea2cf8e9c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
            "compositeImage": {
                "id": "f1141f50-d8d1-4fc2-9afa-de310d88beef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0cbdb25-77a5-4c22-9f7d-88ea2cf8e9c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d361477-5db9-436c-9b24-070ffe7fd1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0cbdb25-77a5-4c22-9f7d-88ea2cf8e9c6",
                    "LayerId": "7731f09e-8e52-4c95-9301-30fbf5f7ba53"
                }
            ]
        },
        {
            "id": "b99ce85a-7151-4be6-98e1-6cc505492436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
            "compositeImage": {
                "id": "897034ef-5dcd-4224-808f-40396b23de2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b99ce85a-7151-4be6-98e1-6cc505492436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e69167a-a260-45c9-84cc-de005e2f0e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b99ce85a-7151-4be6-98e1-6cc505492436",
                    "LayerId": "7731f09e-8e52-4c95-9301-30fbf5f7ba53"
                }
            ]
        },
        {
            "id": "eb93711d-1325-49c2-993e-51989d753ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
            "compositeImage": {
                "id": "fe73edc2-d539-49f0-b084-5ae7e7c3c99c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb93711d-1325-49c2-993e-51989d753ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68e5c2c0-e270-4fed-b517-0544d07846d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb93711d-1325-49c2-993e-51989d753ee1",
                    "LayerId": "7731f09e-8e52-4c95-9301-30fbf5f7ba53"
                }
            ]
        },
        {
            "id": "5fbde14e-28f0-4ea2-877e-bee02d90414c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
            "compositeImage": {
                "id": "f7c31cc1-7799-4fe4-9877-879bd20eab37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbde14e-28f0-4ea2-877e-bee02d90414c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a7f590-759b-48eb-a9f5-b3b959d49bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbde14e-28f0-4ea2-877e-bee02d90414c",
                    "LayerId": "7731f09e-8e52-4c95-9301-30fbf5f7ba53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7731f09e-8e52-4c95-9301-30fbf5f7ba53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}