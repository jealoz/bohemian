{
    "id": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jealoz",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 751,
    "bbox_left": 330,
    "bbox_right": 526,
    "bbox_top": 187,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f8f4455-45af-48c2-880f-c9d52e8d105b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "e3ac3842-3c6b-47eb-b1a9-ab200f3dd59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8f4455-45af-48c2-880f-c9d52e8d105b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619e71e7-b824-4106-a361-5ee048aaa99b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8f4455-45af-48c2-880f-c9d52e8d105b",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "e7a1c002-7da1-495e-b6d0-89717ae691bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "6a257f94-71db-4eef-b024-498c38453e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a1c002-7da1-495e-b6d0-89717ae691bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "565b8cdb-054d-4c5b-a81b-704f4a6cebe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a1c002-7da1-495e-b6d0-89717ae691bc",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "2570eba1-8aee-483b-ad1a-9b91f50bf1bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "38341754-fc2d-4a0a-acd6-a20351d45638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2570eba1-8aee-483b-ad1a-9b91f50bf1bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23eba1bf-2596-48ec-8b44-2cd304375e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2570eba1-8aee-483b-ad1a-9b91f50bf1bb",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "7d3214e5-5b4d-4528-9423-4eac37cd697f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "5f677c36-c402-4b53-bdfa-aab920e125fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d3214e5-5b4d-4528-9423-4eac37cd697f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56bae923-173d-40ba-8807-3226e7def47c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d3214e5-5b4d-4528-9423-4eac37cd697f",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "69441c43-bb3c-4a3b-807d-a278922b6b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "b915a592-e058-4909-8180-673af3168f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69441c43-bb3c-4a3b-807d-a278922b6b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd05595-e02f-40e8-b39d-428f1d2211d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69441c43-bb3c-4a3b-807d-a278922b6b47",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "2a7c0b9d-e060-4949-a8b2-0ce6dbda6e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "8b9c8630-692a-41c4-a191-87148820c0fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a7c0b9d-e060-4949-a8b2-0ce6dbda6e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f631cee-f819-45dd-88aa-a4b16aaba162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a7c0b9d-e060-4949-a8b2-0ce6dbda6e3d",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "fbe35dcc-76a8-4f14-8f82-2c6237c6553c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "dc13fa7f-4de4-4dc5-9fa9-c8843365c1fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe35dcc-76a8-4f14-8f82-2c6237c6553c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36476ff4-58ee-40e2-91c8-1b3e6826b694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe35dcc-76a8-4f14-8f82-2c6237c6553c",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "f1a555de-4361-4907-b59e-cd98d72aa22c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "22f4884b-96f0-4354-839a-af02dc9be7f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a555de-4361-4907-b59e-cd98d72aa22c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b704b6e4-2445-42a2-a7e9-5d28eb5a7e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a555de-4361-4907-b59e-cd98d72aa22c",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "34068eb2-890c-437e-9d86-154b0e311867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "d2780453-afcd-48d6-ab46-6355bd251f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34068eb2-890c-437e-9d86-154b0e311867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a141d8-50ce-4585-b942-74750c3fca50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34068eb2-890c-437e-9d86-154b0e311867",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "a41d350b-7244-42f9-9bc1-ab1899170dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "c9891965-87a7-4bab-a67a-c761257379e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41d350b-7244-42f9-9bc1-ab1899170dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23aa28cf-3e10-4bb5-bdbc-dc17b83c4a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41d350b-7244-42f9-9bc1-ab1899170dc2",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "41cd8c05-39c9-41d8-aa67-7f93fd60e5a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "960f631c-e0b9-4f6c-af27-4d4aa3cd899e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41cd8c05-39c9-41d8-aa67-7f93fd60e5a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fab61a9-3a8b-4a3e-8d59-cf790c09a7a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41cd8c05-39c9-41d8-aa67-7f93fd60e5a0",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "53e5228f-43fb-40f5-8746-4041fd5a5539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "84e60ca0-76c8-4b4b-b5ee-1e24e9d2819a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e5228f-43fb-40f5-8746-4041fd5a5539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6a00aa-48ce-443d-8cbf-796fa5059a02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e5228f-43fb-40f5-8746-4041fd5a5539",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "79433018-431b-43a7-83ca-82bb9adb6094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "944bde7b-6f8f-48aa-a0fe-84b651025a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79433018-431b-43a7-83ca-82bb9adb6094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a55bbb-ae15-454b-ad97-2b49c6562897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79433018-431b-43a7-83ca-82bb9adb6094",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "a29c349d-e3ed-48e9-895b-4fb8a134fe7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "7e4e7e3d-9381-4574-b1ef-61a6b02b1c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a29c349d-e3ed-48e9-895b-4fb8a134fe7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a47c11d-384e-427d-a0a2-d97beefeadbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a29c349d-e3ed-48e9-895b-4fb8a134fe7e",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "d06d4634-fe5d-4c02-b264-d4e39f6eb531",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "b027a56b-0f84-4082-a860-f6518e5e3bc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06d4634-fe5d-4c02-b264-d4e39f6eb531",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c280fff9-4b89-4400-b466-5670ce2d9369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06d4634-fe5d-4c02-b264-d4e39f6eb531",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "2ad10414-2438-4dfa-bac1-5c7bce3c7fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "30e612bf-5707-4e47-a828-283f6f578d49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad10414-2438-4dfa-bac1-5c7bce3c7fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99249fb4-ec8f-4ed3-93a9-ac2ff5a57f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad10414-2438-4dfa-bac1-5c7bce3c7fe6",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "2a0f9c19-1dd4-4aa4-85c9-d974ea2d36d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "c9b24d32-a34f-4d57-9b43-3c07cff76edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0f9c19-1dd4-4aa4-85c9-d974ea2d36d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8d9bb4-48bb-4566-a7ff-003cf7870d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0f9c19-1dd4-4aa4-85c9-d974ea2d36d7",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        },
        {
            "id": "a94866cf-5a70-43c4-883e-ac536b0847a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "compositeImage": {
                "id": "aed1d988-e5b7-40cf-aad1-5cfb373bbdc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94866cf-5a70-43c4-883e-ac536b0847a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc9988c1-fc4c-4737-93bb-f1cbf6c2b482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94866cf-5a70-43c4-883e-ac536b0847a0",
                    "LayerId": "604e0ff2-531d-46fc-84ca-efc7d83be103"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "604e0ff2-531d-46fc-84ca-efc7d83be103",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bb261fd-3a4a-4a1b-ba50-446bcdcdf6cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}