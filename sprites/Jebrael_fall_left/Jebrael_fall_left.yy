{
    "id": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jebrael_fall_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 716,
    "bbox_left": 279,
    "bbox_right": 737,
    "bbox_top": 127,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2af20d9e-bff3-4a22-a5ef-1c214557a51f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "compositeImage": {
                "id": "b22cb5d4-e3ea-4cba-9d3f-a7b5ad472f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2af20d9e-bff3-4a22-a5ef-1c214557a51f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7493d88c-1354-4777-ad3a-d2f282b244c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2af20d9e-bff3-4a22-a5ef-1c214557a51f",
                    "LayerId": "1f84ce6d-fd0d-483f-be71-9b876945628e"
                }
            ]
        },
        {
            "id": "71b1d868-6ca3-4144-b6de-8bfadfae95c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "compositeImage": {
                "id": "90a49021-d654-4443-9045-be21f51a4b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b1d868-6ca3-4144-b6de-8bfadfae95c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2645bb78-03bb-4bd7-a073-11cec8c4079e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b1d868-6ca3-4144-b6de-8bfadfae95c1",
                    "LayerId": "1f84ce6d-fd0d-483f-be71-9b876945628e"
                }
            ]
        },
        {
            "id": "f3ee2150-8030-4c17-83ba-9fcbe4f737cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "compositeImage": {
                "id": "fc27c64e-e854-42c1-b1ed-ce01b91a8e1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ee2150-8030-4c17-83ba-9fcbe4f737cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2508fc97-b5c8-4adc-b4db-4cdb4541ca0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ee2150-8030-4c17-83ba-9fcbe4f737cf",
                    "LayerId": "1f84ce6d-fd0d-483f-be71-9b876945628e"
                }
            ]
        },
        {
            "id": "bbd81ffa-23e5-484a-b2b0-99eac75d7b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "compositeImage": {
                "id": "046fa0c7-7b15-4d09-b0a8-5be3e4521cc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd81ffa-23e5-484a-b2b0-99eac75d7b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5413ad19-d9ae-4156-9802-f70b3414b280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd81ffa-23e5-484a-b2b0-99eac75d7b9f",
                    "LayerId": "1f84ce6d-fd0d-483f-be71-9b876945628e"
                }
            ]
        },
        {
            "id": "1e8c22e7-f098-4e56-8c0f-ccc9bbd1216b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "compositeImage": {
                "id": "3540f20f-d7fe-4793-a449-fd38114bb4d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8c22e7-f098-4e56-8c0f-ccc9bbd1216b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba2ca57-70ef-439f-a4df-09c5d62293bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8c22e7-f098-4e56-8c0f-ccc9bbd1216b",
                    "LayerId": "1f84ce6d-fd0d-483f-be71-9b876945628e"
                }
            ]
        },
        {
            "id": "9d163f77-8970-4748-abd4-749fa3f385d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "compositeImage": {
                "id": "951305eb-ff43-420e-aefc-2aa8c86c318d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d163f77-8970-4748-abd4-749fa3f385d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e72b4ae6-8a86-4a4e-8158-35dc57673ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d163f77-8970-4748-abd4-749fa3f385d0",
                    "LayerId": "1f84ce6d-fd0d-483f-be71-9b876945628e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "1f84ce6d-fd0d-483f-be71-9b876945628e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a5c3415-4602-4b42-9f66-eb22e464b39e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}