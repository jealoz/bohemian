{
    "id": "21b6b33c-f47d-49c0-8e8c-18f8bf086f11",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Basic_platform_game_example",
    "IncludedResources": [
        "Sprites\\sSpinner",
        "Sprites\\sPlayer",
        "Sprites\\sWall",
        "Sprites\\sPlayerHead",
        "Sprites\\sLadders",
        "Sprites\\sPlayerClimbing",
        "Sprites\\sCD",
        "Sprites\\sMetal",
        "Sprites\\sKey",
        "Backgrounds\\bSky",
        "Objects\\control",
        "Objects\\oDangerous",
        "Objects\\oPlayer",
        "Objects\\oWall",
        "Objects\\oSpinner",
        "Objects\\oLadders",
        "Objects\\oDoor",
        "Objects\\oKey",
        "Objects\\oEnd",
        "Rooms\\rmLevel"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 745570506,
    "date": "2019-16-07 01:12:45",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.bygdle.platformexample",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.0"
}