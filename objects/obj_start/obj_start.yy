{
    "id": "a328a288-4e83-41bc-b5d2-3fcde150da68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start",
    "eventList": [
        {
            "id": "0278ea88-711d-44af-8e7f-792218c48786",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "a328a288-4e83-41bc-b5d2-3fcde150da68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "accb3697-ed2d-4b60-a63d-6de53729a597",
    "visible": false
}