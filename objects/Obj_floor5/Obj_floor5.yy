{
    "id": "39ef8f57-1c45-43da-91c4-5f32ec8eb73c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_floor5",
    "eventList": [
        {
            "id": "ea6db7ee-cb05-48e5-9a74-5563460dff41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39ef8f57-1c45-43da-91c4-5f32ec8eb73c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "3dc785dc-1e4b-400e-8e4f-1d201b67b406",
    "visible": true
}