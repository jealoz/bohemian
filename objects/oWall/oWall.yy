{
    "id": "fcaaa3db-1684-4a9f-843c-4fd713b8f9e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWall",
    "eventList": [
        {
            "id": "33851be3-c692-48d3-b1eb-ab292d0898ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fcaaa3db-1684-4a9f-843c-4fd713b8f9e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d44a678b-bf18-414e-a9b7-6bef88147d6f",
    "visible": true
}