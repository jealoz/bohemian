{
    "id": "5af80c90-eee3-4f9a-b9e1-77ac45c85dbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_floor1",
    "eventList": [
        {
            "id": "72ec2d59-75f3-46fa-bbbb-99e48167021b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5af80c90-eee3-4f9a-b9e1-77ac45c85dbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f72bf634-6b93-4b0f-aa28-116dda0792c9",
    "visible": true
}