{
    "id": "1d1d8b04-022f-4dd9-bbe2-3ebbb0516137",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_floor3",
    "eventList": [
        {
            "id": "528f8d66-a385-41c3-b8cd-f8487edf3044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d1d8b04-022f-4dd9-bbe2-3ebbb0516137"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ab7c3676-b134-4c64-9577-3d043a57744b",
    "visible": true
}