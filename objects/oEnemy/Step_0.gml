/// @description Life and die

if life < 0 {life = 0 }
if die = 1 {sprite_index = sEnemyDie }



///Walk
depth = -y
if life > 0 {
if x > oPlayer.x {alarm[2] = 10 }
if x < oPlayer.x {alarm[3] = 10 }
if hspeed > 0 {
image_speed = 0.2
image_xscale = -1
sprite_index = sEnemyWalk }

if hspeed < 0 {
image_speed = 0.2
image_xscale = 1
sprite_index = sEnemyWalk }

if vspeed < 0 and sprite_index = sEnemyIdle and image_xscale = -1{
image_speed = 0.2
image_xscale = -1
sprite_index = sEnemyWalk}

if vspeed < 0 and sprite_index = sEnemyIdle and image_xscale = 1{
image_speed = 0.2
image_xscale = 1
sprite_index = sEnemyWalk}

if vspeed > 0 and sprite_index = sEnemyIdle and image_xscale = -1{
image_speed = 0.2
image_xscale = -1
sprite_index = sEnemyWalk}

if vspeed > 0 and sprite_index = sEnemyIdle and image_xscale = 1{
image_speed = 0.2
image_xscale = 1
sprite_index = sEnemyWalk}

if speed = 0 {
image_speed = 0.1
sprite_index = sEnemyIdle}
//lower limit of the room
if y < 159 {
y = 159
vspeed = 0 }
//upper limit of the room
if y > 210 {
y = 210
vspeed = 0 }
}

/// Walk and attack
if life > 0 {
if conter < 1 {
walk = irandom_range(1,2)
conter = 100 }
if conter > 0 {conter -= 1 }

if walk = 2 and point_distance(x, y, oPlayer.x, oPlayer.y) > 50 and point_distance(x, y, oPlayer.x, oPlayer.y) < 250
   {
   move_towards_point(oPlayer.x, oPlayer.y, 1.5);
   }
else speed = 0; 
// Atack
if conterA < 1 {
attack = irandom_range(1,5)
conterA = 30 }
if conterA > 0 {conterA -= 1 }

if attack = 1 and point_distance(x, y, oPlayer.x, oPlayer.y) < 52
{
    image_speed = 0.1
    sprite_index = sEnemyAttack1
}
if attack = 2 and point_distance(x, y, oPlayer.x, oPlayer.y) < 52
{
    image_speed = 0.1
    sprite_index = sEnemyAttack2
}
if attack = 3 and point_distance(x, y, oPlayer.x, oPlayer.y) < 52
{
    image_index = 1
    sprite_index = sEnemyAttack1
}
if attack = 4 and point_distance(x, y, oPlayer.x, oPlayer.y) < 52
{
    image_index = 1
    sprite_index = sEnemyAttack1
}
if attack = 5 and point_distance(x, y, oPlayer.x, oPlayer.y) < 100 and image_xscale = 1
{
    hspeed = -3
    image_speed = 0.2
    sprite_index = sEnemyRun
}
if attack = 5 and point_distance(x, y, oPlayer.x, oPlayer.y) < 100 and image_xscale = -1
{
    hspeed = 3
    image_speed = 0.2
    sprite_index = sEnemyRun
}}

if life < 1
{
    walk = 6
    attack = 6
    hspeed = 0
    image_speed = 0.05
    die = 1
}

if x > oPlayer.x and oPlayer.image_xscale = 1 and point_distance(x,y,oPlayer.x,oPlayer.y) < 60
if oPlayer.sprite_index = sJumpDown and image_xscale = 1 and mm = 0{
mm = 1
x = x+ 30
image_index = 1
sprite_index = sEnemyAttack1
alarm[0] = 30
exit; }

if x < oPlayer.x and oPlayer.image_xscale = -1 and point_distance(x,y,oPlayer.x,oPlayer.y) < 60
if oPlayer.sprite_index = sJumpDown and image_xscale = -1 and mm = 0{
mm = 1
x = x- 30
alarm[0] = 30
exit; }

