{
    "id": "48a28d93-c574-4991-83d2-25ba9619bfbd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemy",
    "eventList": [
        {
            "id": "3b0b878b-9bc1-4da2-b982-2f453dc8712c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "ea0150ae-1d92-4b6f-b552-daed092e35bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "4ffd922c-b9da-44fb-91ad-a4c6bfa640f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "807c4984-776d-4380-8161-2eb92ae87317",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "4935054b-504c-4d93-8aac-a6594eefb60b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "e5bfbb4d-aa77-436a-b642-d0bda528e9ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "01840ad8-b365-4423-8ff1-7cfd00c6cf53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fd38a736-0413-45e2-89a4-96cd004ed85f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "27dbb642-c4fc-439f-8527-d6919876ede6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        },
        {
            "id": "765dabba-f18a-4bd5-9ba5-47d1686ec7b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "48a28d93-c574-4991-83d2-25ba9619bfbd"
        }
    ],
    "maskSpriteId": "c8f378c0-3454-4804-a7ec-5c890196efd9",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1836692-823f-40b0-8703-c8e7ac9fc3e9",
    "visible": true
}