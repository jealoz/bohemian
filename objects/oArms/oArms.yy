{
    "id": "395b2f20-560e-4ec5-93f8-c7a9b86a23d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oArms",
    "eventList": [
        {
            "id": "a50ccefa-91c2-4f6a-a327-ef8d44804374",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "395b2f20-560e-4ec5-93f8-c7a9b86a23d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47860d47-69b7-4240-ab99-68ba6cf78ca7",
    "visible": true
}