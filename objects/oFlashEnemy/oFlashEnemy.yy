{
    "id": "ad40cd76-7576-4731-aec7-373243bd466d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlashEnemy",
    "eventList": [
        {
            "id": "6ae5209e-0ba3-4d5c-827a-f8d8184a4263",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad40cd76-7576-4731-aec7-373243bd466d"
        },
        {
            "id": "b729a4a2-5b25-44ad-bd8f-377248196fc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5686b852-f85c-477b-9e6b-6806af57f0eb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ad40cd76-7576-4731-aec7-373243bd466d"
        },
        {
            "id": "29d29c55-4a8a-4f4a-b13f-8877c96d87c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ad40cd76-7576-4731-aec7-373243bd466d"
        },
        {
            "id": "b4898dcf-4bdf-4b52-8a15-80a9c7311cb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ad40cd76-7576-4731-aec7-373243bd466d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22a1f827-75cb-48f1-9e69-384805960fc3",
    "visible": true
}