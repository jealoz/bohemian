{
    "id": "58836d16-fd8e-4b50-a898-ed952d806318",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlash2",
    "eventList": [
        {
            "id": "2527c28f-d3cf-4259-9a7e-875b68afab47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "58836d16-fd8e-4b50-a898-ed952d806318"
        },
        {
            "id": "f3a1c31c-3ddd-4fcf-b501-2d2525cf5e6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "58836d16-fd8e-4b50-a898-ed952d806318"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45f1c6f1-9a5d-48d5-95f8-5865ffa806e7",
    "visible": true
}