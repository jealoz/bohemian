{
    "id": "aeee35ab-02ee-4c48-a57c-31ab4140b536",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFire",
    "eventList": [
        {
            "id": "a8df9f03-e343-4507-9aa1-1d1bc711002d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aeee35ab-02ee-4c48-a57c-31ab4140b536"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7696417f-081c-4163-8ecc-df427f5655e8",
    "visible": true
}