/// @description Attack
//Attack 1
if instance_exists(oEnemy ){
if x > enemy.x and sprite_index = sAttack1{
if image_xscale = -1 and point_distance(x,y,enemy.x,enemy.y)< 55
{
    instance_create(x - 35,y-35,oFlash)
    image_speed = 0.1
    sprite_index = sIdle}else{ image_speed = 0.1
    sprite_index = sIdle}}
    
if x < enemy.x and sprite_index = sAttack1{
if image_xscale = 1 and point_distance(x,y,enemy.x,enemy.y)< 55
{
    instance_create(x + 35,y-35,oFlash)
    image_speed = 0.1
    sprite_index = sIdle}else{ image_speed = 0.1
    sprite_index = sIdle}}
//Attack 2
if x > enemy.x and sprite_index = sAttack2{
if image_xscale = -1 and point_distance(x,y,enemy.x,enemy.y)< 55
{
    instance_create(x - 35,y-35,oFlash)
    image_speed = 0.1
    sprite_index = sIdle}else{ image_speed = 0.1
    sprite_index = sIdle}}    
        
if x < enemy.x and sprite_index = sAttack2{
if image_xscale = 1 and point_distance(x,y,enemy.x,enemy.y)< 55
{
    instance_create(x + 35,y-35,oFlash)
    image_speed = 0.1
    sprite_index = sIdle}else{ image_speed = 0.1
    sprite_index = sIdle}}
//Attack 3    
if x > enemy.x and sprite_index = sAttack3{
if  image_xscale = -1 and point_distance(x,y,enemy.x,enemy.y)< 55
{
    instance_create(x - 35,y-35,oFlash)
    image_speed = 0.1
    sprite_index = sIdle}else{ image_speed = 0.1
    sprite_index = sIdle}}    
        
if x < enemy.x and sprite_index = sAttack3{
if image_xscale = 1 and point_distance(x,y,enemy.x,enemy.y)< 55
{
    instance_create(x + 35,y-35,oFlash)
    image_speed = 0.1
    sprite_index = sIdle}else{ image_speed = 0.1
    sprite_index = sIdle}}

if sprite_index = sDefense {
    image_speed = 0.1
    sprite_index = sIdle}
    
if sprite_index = sJumpDown and image_xscale = -1 {
    if point_distance(x,y,enemy.x,enemy.y)< 70 {instance_create(x - 35,y-35,oFlash)}
    image_speed = 0.1
    x = x - 32
    sprite_index = sIdle
    jump = 0}
if sprite_index = sJumpDown and image_xscale = 1 {
    if point_distance(x,y,enemy.x,enemy.y)< 70 {instance_create(x + 35,y-35,oFlash)}
    image_speed = 0.1
    x = x + 32
    sprite_index = sIdle
    jump = 0}
    }

