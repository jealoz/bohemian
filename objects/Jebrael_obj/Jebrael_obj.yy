{
    "id": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Jebrael_obj",
    "eventList": [
        {
            "id": "473c0cec-da55-45d3-a117-cb9ea5ae3486",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65"
        },
        {
            "id": "9699f1e5-48d2-4187-9a81-503dd622068d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65"
        },
        {
            "id": "044676e5-5957-47da-b30b-547bcad32800",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fcaaa3db-1684-4a9f-843c-4fd713b8f9e2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65"
        },
        {
            "id": "3d743387-b69f-4d9d-b949-c11604e749ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5af80c90-eee3-4f9a-b9e1-77ac45c85dbb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65"
        },
        {
            "id": "17589235-95c7-4d02-90b1-d4f379fc23e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5c735cae-9a40-4807-9acb-c31759ad516e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65"
        },
        {
            "id": "8b19dfaf-d4b8-4213-96c8-146ad99aa387",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1d1d8b04-022f-4dd9-bbe2-3ebbb0516137",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65"
        }
    ],
    "maskSpriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b926432-229c-456d-afb2-05fca8de70ae",
    "visible": true
}