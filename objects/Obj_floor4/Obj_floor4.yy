{
    "id": "920d307c-4cc3-4952-be62-3e927766b5ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_floor4",
    "eventList": [
        {
            "id": "e038a80e-c9fe-4300-a126-4c10b8047bee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "920d307c-4cc3-4952-be62-3e927766b5ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "54c005c9-005b-41bd-9b03-564bf5194ec0",
    "visible": true
}