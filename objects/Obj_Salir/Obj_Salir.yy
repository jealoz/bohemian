{
    "id": "a8ce279b-0cea-4ed3-9f96-7b1d79f3a590",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Salir",
    "eventList": [
        {
            "id": "a47981b0-1242-4a15-8474-25e5124206db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "a8ce279b-0cea-4ed3-9f96-7b1d79f3a590"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "accb3697-ed2d-4b60-a63d-6de53729a597",
    "visible": false
}