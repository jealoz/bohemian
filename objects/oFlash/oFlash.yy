{
    "id": "fd38a736-0413-45e2-89a4-96cd004ed85f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlash",
    "eventList": [
        {
            "id": "21489d16-4d02-4934-8e11-757b1a1f722b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd38a736-0413-45e2-89a4-96cd004ed85f"
        },
        {
            "id": "d3659df8-af1e-47d8-8445-fdd6ee0c0463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "fd38a736-0413-45e2-89a4-96cd004ed85f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22a1f827-75cb-48f1-9e69-384805960fc3",
    "visible": true
}