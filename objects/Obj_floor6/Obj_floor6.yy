{
    "id": "f1370bf0-b619-4950-92a4-358e996e8484",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_floor6",
    "eventList": [
        {
            "id": "d1d72b59-6f97-4d5a-912e-7d7775dca18d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f1370bf0-b619-4950-92a4-358e996e8484"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6d9d3b9c-421f-4ea7-b3ed-647c5b0c3fc0",
    "visible": true
}