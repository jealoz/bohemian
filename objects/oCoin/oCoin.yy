{
    "id": "4a67468f-d13c-4bf3-aa99-eb067e4d58bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCoin",
    "eventList": [
        {
            "id": "b50996be-342d-4d82-aa42-d7d6085dce35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a67468f-d13c-4bf3-aa99-eb067e4d58bc"
        },
        {
            "id": "ed55047a-956c-45ab-9fda-b58b87f29ad7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a67468f-d13c-4bf3-aa99-eb067e4d58bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aaf8552e-cdde-4768-8ad8-cbabc9bc0019",
    "visible": true
}