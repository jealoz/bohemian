{
    "id": "5c735cae-9a40-4807-9acb-c31759ad516e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_floor2",
    "eventList": [
        {
            "id": "6ef0efb1-2a55-45c1-9672-e7594b2c4498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c735cae-9a40-4807-9acb-c31759ad516e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "68079e4e-079b-4f10-b2de-2435ec482fe1",
    "visible": true
}