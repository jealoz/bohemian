
{
    "name": "room0",
    "id": "6b68512d-0b6d-4d93-a314-44cb2748d6ec",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "03a73911-ffef-4f92-9050-9c57974142ab",
        "7fa7c8fe-537f-4abc-a08e-a2b035511aa7",
        "13083fc0-dccf-455c-acac-9598eafefbf9",
        "897a9328-72e4-43d1-a4e8-25504f16bb9f",
        "cb3378a0-6f46-4975-a3ec-99f4ae101354",
        "ebab798d-f958-40b7-bd92-f65d80be5d83",
        "5de78395-a832-4052-9cca-f774409fff8a",
        "7a7ea951-4a00-4330-adfd-4bfd7d9e8c37",
        "8d6cc9ba-4453-4798-a020-53891d83f7dd",
        "60150457-1b15-4233-9a02-d4298e19c26d",
        "0b7f5586-5d90-4c1f-95e2-e7905437511f",
        "8851109a-729e-488c-ac7b-84ffff894f84",
        "e7171427-acbf-4dab-8ea2-b4d06931ba32",
        "abe4dc4e-2eb9-4fff-8668-cf99f09fa662",
        "1ea95b7e-4509-4da8-90c8-6293bd69697c",
        "e1ca85e0-5053-4241-a91c-f1cff6e00d93",
        "a810e510-5254-44d9-8aa0-307631efc064",
        "7cff5bb1-c6c4-42f2-a50b-5d3b31cd7a63",
        "8917127d-5093-4fb9-9ef7-cea2017aee69"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances_1",
            "id": "99bc9f62-c22e-4402-a466-e8eaa861ba5e",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_316FA396","id": "7fa7c8fe-537f-4abc-a08e-a2b035511aa7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_316FA396","objId": "5af80c90-eee3-4f9a-b9e1-77ac45c85dbb","properties": null,"rotation": 0,"scaleX": 15,"scaleY": 15,"mvc": "1.1","x": 832,"y": 2400},
{"name": "inst_30C6D92C","id": "13083fc0-dccf-455c-acac-9598eafefbf9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_30C6D92C","objId": "39ef8f57-1c45-43da-91c4-5f32ec8eb73c","properties": null,"rotation": 0,"scaleX": 15,"scaleY": 15,"mvc": "1.1","x": 864,"y": 3104},
{"name": "inst_4C596FC7","id": "897a9328-72e4-43d1-a4e8-25504f16bb9f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4C596FC7","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 1861,"y": 2400},
{"name": "inst_7B711AE0","id": "cb3378a0-6f46-4975-a3ec-99f4ae101354","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7B711AE0","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 2912,"y": 2400},
{"name": "inst_476D4ED4","id": "ebab798d-f958-40b7-bd92-f65d80be5d83","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_476D4ED4","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 15.5,"scaleY": 15,"mvc": "1.1","x": 9184,"y": 2368},
{"name": "inst_55DA74BB","id": "5de78395-a832-4052-9cca-f774409fff8a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_55DA74BB","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 1614.5,"y": 3200},
{"name": "inst_937B87A","id": "7a7ea951-4a00-4330-adfd-4bfd7d9e8c37","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_937B87A","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 2656,"y": 3200},
{"name": "inst_49BA7D94","id": "8d6cc9ba-4453-4798-a020-53891d83f7dd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_49BA7D94","objId": "f1370bf0-b619-4950-92a4-358e996e8484","properties": null,"rotation": 0,"scaleX": 21.5,"scaleY": 15,"mvc": "1.1","x": 8768,"y": 3040},
{"name": "inst_77B3DD78","id": "60150457-1b15-4233-9a02-d4298e19c26d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_77B3DD78","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 3968,"y": 2400},
{"name": "inst_7DF18B15","id": "0b7f5586-5d90-4c1f-95e2-e7905437511f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7DF18B15","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 5024,"y": 2400},
{"name": "inst_C50E3D7","id": "8851109a-729e-488c-ac7b-84ffff894f84","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_C50E3D7","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 6080,"y": 2400},
{"name": "inst_7621248F","id": "e7171427-acbf-4dab-8ea2-b4d06931ba32","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7621248F","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 7136,"y": 2400},
{"name": "inst_55A68493","id": "abe4dc4e-2eb9-4fff-8668-cf99f09fa662","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_55A68493","objId": "5c735cae-9a40-4807-9acb-c31759ad516e","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 8192,"y": 2400},
{"name": "inst_665A209B","id": "1ea95b7e-4509-4da8-90c8-6293bd69697c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_665A209B","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 3712,"y": 3200},
{"name": "inst_508E5803","id": "e1ca85e0-5053-4241-a91c-f1cff6e00d93","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_508E5803","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 4768,"y": 3200},
{"name": "inst_6C017D68","id": "a810e510-5254-44d9-8aa0-307631efc064","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6C017D68","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 5824,"y": 3200},
{"name": "inst_9608593","id": "7cff5bb1-c6c4-42f2-a50b-5d3b31cd7a63","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_9608593","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 6880,"y": 3200},
{"name": "inst_801F362","id": "8917127d-5093-4fb9-9ef7-cea2017aee69","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_801F362","objId": "920d307c-4cc3-4952-be62-3e927766b5ee","properties": null,"rotation": 0,"scaleX": 16.5,"scaleY": 15,"mvc": "1.1","x": 7936,"y": 3200}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "0d709bf1-ff08-4df8-8032-d8ee2ed60684",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_6E24ED40","id": "03a73911-ffef-4f92-9050-9c57974142ab","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6E24ED40","objId": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65","properties": null,"rotation": 0,"scaleX": 0.6711109,"scaleY": 0.9155556,"mvc": "1.1","x": 672,"y": 448}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "2e05b141-930b-4453-8aab-9eb4051c301c",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "731ff4a8-48d0-4253-aa71-344f6c7f92ce",
            "stretch": true,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "439aeeee-25d5-4225-ada4-93d1be78d77b",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "ca7d427d-e45d-4e95-94e3-b14e840cceaf",
        "Height": 4000,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 12000
    },
    "mvc": "1.0",
    "views": [
{"id": "b0b30f3e-5c6b-4d8a-87cc-c5f460f96d1f","hborder": 1,"hport": 768,"hspeed": -1,"hview": 3900,"inherit": false,"modelName": "GMRView","objId": "68ceb07b-821e-4fe8-8eb2-c47d3cbc7f65","mvc": "1.0","vborder": 1,"visible": true,"vspeed": -1,"wport": 1024,"wview": 3200,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "dd45c27d-b29f-444c-9557-96c02d728a2f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a4e3636a-0af9-4198-ab98-2666ef146ad4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a8d1502c-11b9-4df5-8672-f3937849ff87","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1b61eb03-f21f-4878-ac09-ab4982b7cd9b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ed32538f-7080-4e7c-9673-5f3c55864adb","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ea492f2e-8190-4e91-bab5-65fb54defa94","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "957c97a6-1f2b-4614-8e95-4b410b68c361","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "7e628b05-7ea4-4508-8c28-f1eeb4a1f097",
        "clearDisplayBuffer": true,
        "clearViewBackground": true,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}